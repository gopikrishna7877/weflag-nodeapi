import * as express from 'express';
import * as bodyParser from "body-parser";
import { verify, sign } from 'jsonwebtoken';
import * as mail from 'nodemailer';
import * as jobSeekerRoute from './jobseeker/api';
import * as dashBoardRoute from './dashboard/api';
import * as recuriterRoute from './recruiter/editapi';
import * as creditsRoute from './credits/api';
import { JWT_SECRET } from './utils/jwt';
import * as adminRoute from './adminAPI/api';


let app = express();
app.use(bodyParser.json());

// app.use(async (req, res, next) => {
//     let apiKey;
//     if (req.headers && req.headers.authorization) {
//         const parts = req.headers.authorization.split(' ');
//         if (parts.length === 2 && parts[0] === 'Bearer') {
//             apiKey = parts[1];
//         }
//     }
//     if (apiKey) {
//         verify(apiKey, JWT_SECRET, async (error, decoded) => {
//             if (error) {
//                 return res.send(error.message);
//             }
//             //  req.token = decoded.user;
//             next();
//         })
//     }
//     else {
//         return res.send("This access token is not valid anymore. Please login.", 401);
//     }
// })
app.use('/jobSeeker', jobSeekerRoute);
app.use('/credits', creditsRoute);
app.use('/dashBoard', dashBoardRoute);
app.use('/recuriter', recuriterRoute);
app.use('/admin', adminRoute)

export = app;
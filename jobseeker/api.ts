import { Router } from 'express';
import * as formidable from 'formidable';
import * as fs from 'fs-extra';
let router = Router();
import { Status } from '../utils/status';
import { APIError } from '../utils/APIError';
import { jobSeekerSchema } from '../modals/jobseeker';
import { flagDetailsSchema } from '../modals/flagdetails';
import { lookUpSchema } from '../modals/lookup';
import * as mongoose from 'mongoose';
import { companyIdMsg, fstsIdMsg, recuriterIdMsg, nameMsg, emailMsg, contactNumberMsg, companyNameMsg, whtMsg, errorInSaveMsg, jobSeekerExistsMsg, jobSeekerIdMsg, jobSeekerNotExistsMsg, noShowCrType, noShowCredits, notJoinedCredits, notJoinedCrType, joinedCredits, joinedCrType, serachCredits, searchCrType, flagIdMsg } from '../utils/errorMsg';
import { transtionCreditsSchema } from '../modals/walletcredits';
import { companySchema } from '../modals/company';
import { recuriterSchema } from '../modals/recruiters';
import { attachmentSchema } from '../modals/attachment';
import { crLookSchema } from '../modals/creditslookup';
let body;
router.post("/getJobSeekerList", async (req, res, next) => {
    // let list = []
    try {
        body = req.body;
        if (!body.cid) {
            res.send(new APIError(201, companyIdMsg))
        }
        else if (!body.fsts) {
            res.send(new APIError(201, fstsIdMsg))
        }
        else {
            let jobseekerList: any = await jobSeekerSchema.find({
                cid: mongoose.Types.ObjectId(body.cid),
                fsts: body.fsts,
                active: 1
            }).sort({ createdAt: -1 }).exec();
            if (jobseekerList.length > 0) {
                await jobseekerList.forEach(async ele => {
                    ele.fstsCnt = await flagDetailsSchema.find({
                        "jid": ele._id,
                        active: 1,
                        fsts: body.fsts
                    }).countDocuments();
                    // list.push(ele);
                });
                setTimeout(() => {
                    res.send(new Status(200, jobseekerList));
                }, 100);
            }
            else {
                res.send(new Status(200, jobseekerList));
            }
        }

    } catch (error) {
        res.send(new APIError(201, error))
    }

})

router.post("/addJobSeeker", async (req, res, next) => {
    try {
        body = req.body;
        if (!body.cid) {
            res.send(new APIError(201, companyIdMsg))
        }
        else if (!body.rid) {
            res.send(new APIError(201, recuriterIdMsg))
        }
        else if (!body.jsname.trim()) {
            res.send(new APIError(201, nameMsg))
        }
        else if (!body.jsemail) {
            res.send(new APIError(201, emailMsg))
        }
        else if (!body.jscon) {
            res.send(new APIError(201, contactNumberMsg))
        }
        else if (!body.cname.trim()) {
            res.send(new APIError(201, companyNameMsg))
        }
        else if (!body.fsts) {
            res.send(new APIError(201, fstsIdMsg))
        }
        else if (!body.whd.trim()) {
            res.send(new APIError(201, whtMsg))
        }
        else {
            // let isExitJobSeeker = await jobSeekerSchema.find({
            //     jsemail: body.jsemail.toLowerCase(),
            //     jscon: body.jscon,
            //     jsaorp:body.jsaorp,
            //     active: 1,
            // }).exec();
            let query;
            if (body.jsaorp) {
                query = [
                    {
                        jsemail: body.jsemail.toLowerCase()
                    },
                    {
                        jscon: body.jscon
                    },
                    {
                        jsaorp: body.jsaorp ? body.jsaorp.toLowerCase() : null
                    }
                ]
            }
            else {
                query = [
                    {
                        jsemail: body.jsemail.toLowerCase()
                    },
                    {
                        jscon: body.jscon
                    }
                ]
            }

            let isExitJobSeeker = await jobSeekerSchema.find({ $or: query }, { active: 1 }).exec();
            if (isExitJobSeeker.length == 0) {
                let newJobSeeker = new jobSeekerSchema({
                    cid: mongoose.Types.ObjectId(body.cid),
                    rid: mongoose.Types.ObjectId(body.rid),
                    jsname: body.jsname,
                    jsemail: body.jsemail.toLowerCase(),
                    jscon: body.jscon,
                    jsalcon: body.jsalcon,
                    jsaorp: body.jsaorp ? body.jsaorp.toLowerCase() : null,
                    cdate: Date.now(),
                    fsts: body.fsts
                });
                let jobSeekerDetails: any = await newJobSeeker.save();
                if (jobSeekerDetails) {
                    body.jid = jobSeekerDetails._id;
                    let newFlag = await insertFlag(body)
                    let newFlagDetails = await newFlag.save();
                    //For Credits
                    insertOrUpdateCredits(body);
                    //For Credits
                    if (newFlagDetails) {
                        res.send(new Status(200, newFlagDetails));
                    }
                    else {
                        res.send(new APIError(201, errorInSaveMsg))
                    }
                }
                else {
                    res.send(new APIError(201, errorInSaveMsg))
                }

            }
            else {
                res.send(new APIError(201, jobSeekerExistsMsg))
            }
        }
    } catch (error) {
        res.send(new APIError(201, error))
    }

})

async function insertFlag(body) {
    let newFlag = new flagDetailsSchema({
        cid: mongoose.Types.ObjectId(body.cid),
        rid: mongoose.Types.ObjectId(body.rid),
        jid: mongoose.Types.ObjectId(body.jid),
        cdate: Date.now(),
        fsts: body.fsts,
        cname: body.cname,
        isd: body.isd,
        whd: body.whd,
        ord: body.ord,
        oad: body.oad,
        pjd: body.pjd,
        infdt: body.infdt,
        idt: body.idt,
        ajdt: body.ajdt
    });
    return newFlag;
}

async function insertOrUpdateCredits(body) {
    //For Credits
    let company: any = await companySchema.findOne({ _id: mongoose.Types.ObjectId(body.cid) }).exec();
    if (company) {
        let recruiter: any = await recuriterSchema.findOne({ _id: mongoose.Types.ObjectId(body.rid) }).exec();
        if (recruiter) {
            let jobseeker: any = await jobSeekerSchema.findOne({ _id: mongoose.Types.ObjectId(body.jid) }).exec();
            if (jobseeker) {
                let crL:any = await crLookSchema.find({}).exec();
                if (crL.length > 0) {
                    let creditPoint;
                    let creditType;
                    if (body.fsts == 1) {
                        creditPoint = crL[0].noshow //noShowCredits;
                        creditType = noShowCrType
                    }
                    else if (body.fsts == 2) {
                        creditPoint = crL[0].notjoined //notJoinedCredits;
                        creditType = notJoinedCrType

                    }
                    else if (body.fsts == 3) {
                        creditPoint = crL[0].joined //joinedCredits;
                        creditType = joinedCrType
                    }

                    let newCredits = company.credits + creditPoint;
                    let uCompany = await companySchema.updateOne(
                        {
                            _id: mongoose.Types.ObjectId(body.cid)
                        },
                        {
                            $set: {
                                credits: newCredits
                            }
                        }
                    );
                    let crTrn = new transtionCreditsSchema({
                        cid: body.cid,
                        rid: body.rid,
                        jid: jobseeker._id,
                        credits: creditPoint,
                        rname: recruiter.rname,
                        rcon: recruiter.con,
                        jsname: jobseeker.jsname,
                        trtype: creditType
                    })
                    return crTrn.save();
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }

    }
    else {
        return false;
    }
    //For Credits
}

router.post("/flagDetails", async (req, res, next) => {
    try {
        body = req.body;
        if (!body.jid) {
            res.send(new APIError(201, jobSeekerIdMsg))
        }
        else if (!body.fsts) {
            res.send(new APIError(201, fstsIdMsg))
        }
        else {
            let jobseeker = await jobSeekerSchema.findById(mongoose.Types.ObjectId(body.jid)).exec();
            if (jobseeker) {
                let flagList
                if (body.fsts == 4) {
                    flagList = await flagDetailsSchema.find({ jid: mongoose.Types.ObjectId(body.jid) }).sort({ createdAt: -1 }).exec();
                }
                else {
                    flagList = await flagDetailsSchema.find({ jid: mongoose.Types.ObjectId(body.jid), fsts: body.fsts }).sort({ createdAt: -1 }).exec();
                }
                let obj = {
                    jobSeekerDetails: jobseeker,
                    flagDetails: []
                };
                
                if (flagList.length > 0) {
                    for (let d = 0; d < flagList.length; d++) {
                        let attch=await attachmentSchema.find({fid:flagList[d]._id}).exec();
                        flagList[d]._doc.attachment=attch;
                        obj.flagDetails = flagList;
                    }
                   
                }

                res.send(new Status(200, obj))
            }
            else {
                res.send(new APIError(201, jobSeekerNotExistsMsg))
            }
        }
    } catch (error) {
        res.send(new APIError(201, error))
    }
});

router.post("/addFlag", async (req, res, next) => {
    try {
        body = req.body;
        if (!body.cid) {
            res.send(new APIError(201, companyIdMsg))
        }
        else if (!body.rid) {
            res.send(new APIError(201, recuriterIdMsg))
        }
        else if (!body.jid) {
            res.send(new APIError(201, jobSeekerIdMsg))
        }
        else if (!body.cname.trim()) {
            res.send(new APIError(201, companyNameMsg))
        }
        else if (!body.fsts) {
            res.send(new APIError(201, fstsIdMsg))
        }
        else if (!body.whd.trim()) {
            res.send(new APIError(201, whtMsg))
        }
        else {
            let newFlag = await insertFlag(body)
            let newFlagDetails = await newFlag.save();
            if (newFlagDetails) {
                //For Credits
                insertOrUpdateCredits(body);
                //For Credits
                res.send(new Status(200, newFlagDetails));
            }
            else {
                res.send(new APIError(201, errorInSaveMsg))
            }

        }

    } catch (error) {
        res.send(new APIError(201, error))
    }

});
router.post("/deleteJobSeeker", async (req, res, next) => {
    try {
        body = req.body;
        if (!body.jid) {
            res.send(new APIError(201, jobSeekerIdMsg));
        }
        else {
            let deleteJobSeeker = await jobSeekerSchema.updateOne({ "_id": mongoose.Types.ObjectId(body.jid) }, { $set: { active: 0 } }).exec();
            res.send(new Status(200, deleteJobSeeker));
        }
    } catch (error) {
        res.send(new APIError(201, error))
    }
});

router.post("/status", async (req, res, next) => {
    try {
        let lookList = await lookUpSchema.find({}).exec();
        res.send(new Status(200, lookList))
    } catch (error) {
        res.send(new APIError(201, error))
    }
});


router.post("/flagAttachement", async (req, res, next) => {
    var form = new formidable.IncomingForm();
    form.parse(req, (err, fields, files) => {
        //  res.writeHead(200, { 'content-type': 'text/plain' });
        let body = fields;
        if (!body.cid) {
            res.send(new APIError(201, companyIdMsg));
        }
        else if (!body.rid) {
            res.send(new APIError(201, recuriterIdMsg));
        }
        else if (!body.jid) {
            res.send(new APIError(201, jobSeekerIdMsg));
        }
        else if (!body.fid) {
            res.send(new APIError(201, flagIdMsg));
        }
        else {
            let fList:any = Object.values(files);
            let cnt = 0;
            let newAchmnt;
            for (let f = 0; f < fList.length; f++) {
                fs.readFile(fList[f].path, async (err, data) => {
                    if (err) { res.send(new APIError(201, err)); }
                    let urlPath = './Attachment/' + "cid-" + body.cid + "/" + "rid-" + body.rid + "/" + "jid-" + body.jid+ "/" + "fid-" + body.fid;
                    let dbPath = '' + "cid-" + body.cid + "/" + "rid-" + body.rid + "/" + "jid-" + body.jid + "/" + "fid-" + body.fid + "/";
                    if (!fs.existsSync(urlPath)) {
                        try {
                            fs.mkdirSync(urlPath, { recursive: true });
                        } catch (error) {
                            console.log(error.message);
                        }
                    }
                    fs.writeFile(urlPath + "/" + fList[f].name, data, async (err) => {
                      
                        if (err) {
                            console.log(err)
                            res.send(new APIError(201, err));
                        }
                        else {
                            try {
                                let achmnt = new attachmentSchema({
                                    cid: body.cid,
                                    rid: body.rid,
                                    jid: body.jid,
                                    fid: body.fid,
                                    path: dbPath + fList[f].name
                                });
                                newAchmnt = await achmnt.save();
                                cnt++;
                                if (cnt == fList.length) {
                                    res.send(new Status(200, newAchmnt))
                                }

                            } catch (error) {
                                res.send(new APIError(201, error))
                            }
                        }
                    });
                    //Delete the file
                    fs.unlink(fList[f].path, function (err) {
                        if (err) res.send(new APIError(201, err));
                        // console.log('File deleted!');
                    });

                });

            }

        }
    })
});



export =router;


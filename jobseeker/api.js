"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var express_1 = require("express");
var formidable = require("formidable");
var fs = require("fs-extra");
var router = express_1.Router();
var status_1 = require("../utils/status");
var APIError_1 = require("../utils/APIError");
var jobseeker_1 = require("../modals/jobseeker");
var flagdetails_1 = require("../modals/flagdetails");
var lookup_1 = require("../modals/lookup");
var mongoose = require("mongoose");
var errorMsg_1 = require("../utils/errorMsg");
var walletcredits_1 = require("../modals/walletcredits");
var company_1 = require("../modals/company");
var recruiters_1 = require("../modals/recruiters");
var attachment_1 = require("../modals/attachment");
var creditslookup_1 = require("../modals/creditslookup");
var body;
router.post("/getJobSeekerList", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var jobseekerList_1, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 7, , 8]);
                body = req.body;
                if (!!body.cid) return [3 /*break*/, 1];
                res.send(new APIError_1.APIError(201, errorMsg_1.companyIdMsg));
                return [3 /*break*/, 6];
            case 1:
                if (!!body.fsts) return [3 /*break*/, 2];
                res.send(new APIError_1.APIError(201, errorMsg_1.fstsIdMsg));
                return [3 /*break*/, 6];
            case 2: return [4 /*yield*/, jobseeker_1.jobSeekerSchema.find({
                    cid: mongoose.Types.ObjectId(body.cid),
                    fsts: body.fsts,
                    active: 1
                }).sort({ createdAt: -1 }).exec()];
            case 3:
                jobseekerList_1 = _a.sent();
                if (!(jobseekerList_1.length > 0)) return [3 /*break*/, 5];
                return [4 /*yield*/, jobseekerList_1.forEach(function (ele) { return __awaiter(void 0, void 0, void 0, function () {
                        var _a;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ele;
                                    return [4 /*yield*/, flagdetails_1.flagDetailsSchema.find({
                                            "jid": ele._id,
                                            active: 1,
                                            fsts: body.fsts
                                        }).countDocuments()];
                                case 1:
                                    _a.fstsCnt = _b.sent();
                                    return [2 /*return*/];
                            }
                        });
                    }); })];
            case 4:
                _a.sent();
                setTimeout(function () {
                    res.send(new status_1.Status(200, jobseekerList_1));
                }, 100);
                return [3 /*break*/, 6];
            case 5:
                res.send(new status_1.Status(200, jobseekerList_1));
                _a.label = 6;
            case 6: return [3 /*break*/, 8];
            case 7:
                error_1 = _a.sent();
                res.send(new APIError_1.APIError(201, error_1));
                return [3 /*break*/, 8];
            case 8: return [2 /*return*/];
        }
    });
}); });
router.post("/addJobSeeker", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var query, isExitJobSeeker, newJobSeeker, jobSeekerDetails, newFlag, newFlagDetails, error_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 17, , 18]);
                body = req.body;
                if (!!body.cid) return [3 /*break*/, 1];
                res.send(new APIError_1.APIError(201, errorMsg_1.companyIdMsg));
                return [3 /*break*/, 16];
            case 1:
                if (!!body.rid) return [3 /*break*/, 2];
                res.send(new APIError_1.APIError(201, errorMsg_1.recuriterIdMsg));
                return [3 /*break*/, 16];
            case 2:
                if (!!body.jsname.trim()) return [3 /*break*/, 3];
                res.send(new APIError_1.APIError(201, errorMsg_1.nameMsg));
                return [3 /*break*/, 16];
            case 3:
                if (!!body.jsemail) return [3 /*break*/, 4];
                res.send(new APIError_1.APIError(201, errorMsg_1.emailMsg));
                return [3 /*break*/, 16];
            case 4:
                if (!!body.jscon) return [3 /*break*/, 5];
                res.send(new APIError_1.APIError(201, errorMsg_1.contactNumberMsg));
                return [3 /*break*/, 16];
            case 5:
                if (!!body.cname.trim()) return [3 /*break*/, 6];
                res.send(new APIError_1.APIError(201, errorMsg_1.companyNameMsg));
                return [3 /*break*/, 16];
            case 6:
                if (!!body.fsts) return [3 /*break*/, 7];
                res.send(new APIError_1.APIError(201, errorMsg_1.fstsIdMsg));
                return [3 /*break*/, 16];
            case 7:
                if (!!body.whd.trim()) return [3 /*break*/, 8];
                res.send(new APIError_1.APIError(201, errorMsg_1.whtMsg));
                return [3 /*break*/, 16];
            case 8:
                query = void 0;
                if (body.jsaorp) {
                    query = [
                        {
                            jsemail: body.jsemail.toLowerCase()
                        },
                        {
                            jscon: body.jscon
                        },
                        {
                            jsaorp: body.jsaorp ? body.jsaorp.toLowerCase() : null
                        }
                    ];
                }
                else {
                    query = [
                        {
                            jsemail: body.jsemail.toLowerCase()
                        },
                        {
                            jscon: body.jscon
                        }
                    ];
                }
                return [4 /*yield*/, jobseeker_1.jobSeekerSchema.find({ $or: query }, { active: 1 }).exec()];
            case 9:
                isExitJobSeeker = _a.sent();
                if (!(isExitJobSeeker.length == 0)) return [3 /*break*/, 15];
                newJobSeeker = new jobseeker_1.jobSeekerSchema({
                    cid: mongoose.Types.ObjectId(body.cid),
                    rid: mongoose.Types.ObjectId(body.rid),
                    jsname: body.jsname,
                    jsemail: body.jsemail.toLowerCase(),
                    jscon: body.jscon,
                    jsalcon: body.jsalcon,
                    jsaorp: body.jsaorp ? body.jsaorp.toLowerCase() : null,
                    cdate: Date.now(),
                    fsts: body.fsts
                });
                return [4 /*yield*/, newJobSeeker.save()];
            case 10:
                jobSeekerDetails = _a.sent();
                if (!jobSeekerDetails) return [3 /*break*/, 13];
                body.jid = jobSeekerDetails._id;
                return [4 /*yield*/, insertFlag(body)];
            case 11:
                newFlag = _a.sent();
                return [4 /*yield*/, newFlag.save()];
            case 12:
                newFlagDetails = _a.sent();
                //For Credits
                insertOrUpdateCredits(body);
                //For Credits
                if (newFlagDetails) {
                    res.send(new status_1.Status(200, newFlagDetails));
                }
                else {
                    res.send(new APIError_1.APIError(201, errorMsg_1.errorInSaveMsg));
                }
                return [3 /*break*/, 14];
            case 13:
                res.send(new APIError_1.APIError(201, errorMsg_1.errorInSaveMsg));
                _a.label = 14;
            case 14: return [3 /*break*/, 16];
            case 15:
                res.send(new APIError_1.APIError(201, errorMsg_1.jobSeekerExistsMsg));
                _a.label = 16;
            case 16: return [3 /*break*/, 18];
            case 17:
                error_2 = _a.sent();
                res.send(new APIError_1.APIError(201, error_2));
                return [3 /*break*/, 18];
            case 18: return [2 /*return*/];
        }
    });
}); });
function insertFlag(body) {
    return __awaiter(this, void 0, void 0, function () {
        var newFlag;
        return __generator(this, function (_a) {
            newFlag = new flagdetails_1.flagDetailsSchema({
                cid: mongoose.Types.ObjectId(body.cid),
                rid: mongoose.Types.ObjectId(body.rid),
                jid: mongoose.Types.ObjectId(body.jid),
                cdate: Date.now(),
                fsts: body.fsts,
                cname: body.cname,
                isd: body.isd,
                whd: body.whd,
                ord: body.ord,
                oad: body.oad,
                pjd: body.pjd,
                infdt: body.infdt,
                idt: body.idt,
                ajdt: body.ajdt
            });
            return [2 /*return*/, newFlag];
        });
    });
}
function insertOrUpdateCredits(body) {
    return __awaiter(this, void 0, void 0, function () {
        var company, recruiter, jobseeker, crL, creditPoint, creditType, newCredits, uCompany, crTrn;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, company_1.companySchema.findOne({ _id: mongoose.Types.ObjectId(body.cid) }).exec()];
                case 1:
                    company = _a.sent();
                    if (!company) return [3 /*break*/, 12];
                    return [4 /*yield*/, recruiters_1.recuriterSchema.findOne({ _id: mongoose.Types.ObjectId(body.rid) }).exec()];
                case 2:
                    recruiter = _a.sent();
                    if (!recruiter) return [3 /*break*/, 10];
                    return [4 /*yield*/, jobseeker_1.jobSeekerSchema.findOne({ _id: mongoose.Types.ObjectId(body.jid) }).exec()];
                case 3:
                    jobseeker = _a.sent();
                    if (!jobseeker) return [3 /*break*/, 8];
                    return [4 /*yield*/, creditslookup_1.crLookSchema.find({}).exec()];
                case 4:
                    crL = _a.sent();
                    if (!(crL.length > 0)) return [3 /*break*/, 6];
                    creditPoint = void 0;
                    creditType = void 0;
                    if (body.fsts == 1) {
                        creditPoint = crL[0].noshow; //noShowCredits;
                        creditType = errorMsg_1.noShowCrType;
                    }
                    else if (body.fsts == 2) {
                        creditPoint = crL[0].notjoined; //notJoinedCredits;
                        creditType = errorMsg_1.notJoinedCrType;
                    }
                    else if (body.fsts == 3) {
                        creditPoint = crL[0].joined; //joinedCredits;
                        creditType = errorMsg_1.joinedCrType;
                    }
                    newCredits = company.credits + creditPoint;
                    return [4 /*yield*/, company_1.companySchema.updateOne({
                            _id: mongoose.Types.ObjectId(body.cid)
                        }, {
                            $set: {
                                credits: newCredits
                            }
                        })];
                case 5:
                    uCompany = _a.sent();
                    crTrn = new walletcredits_1.transtionCreditsSchema({
                        cid: body.cid,
                        rid: body.rid,
                        jid: jobseeker._id,
                        credits: creditPoint,
                        rname: recruiter.rname,
                        rcon: recruiter.con,
                        jsname: jobseeker.jsname,
                        trtype: creditType
                    });
                    return [2 /*return*/, crTrn.save()];
                case 6: return [2 /*return*/, false];
                case 7: return [3 /*break*/, 9];
                case 8: return [2 /*return*/, false];
                case 9: return [3 /*break*/, 11];
                case 10: return [2 /*return*/, false];
                case 11: return [3 /*break*/, 13];
                case 12: return [2 /*return*/, false];
                case 13: return [2 /*return*/];
            }
        });
    });
}
router.post("/flagDetails", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var jobseeker, flagList, obj, d, attch, error_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 14, , 15]);
                body = req.body;
                if (!!body.jid) return [3 /*break*/, 1];
                res.send(new APIError_1.APIError(201, errorMsg_1.jobSeekerIdMsg));
                return [3 /*break*/, 13];
            case 1:
                if (!!body.fsts) return [3 /*break*/, 2];
                res.send(new APIError_1.APIError(201, errorMsg_1.fstsIdMsg));
                return [3 /*break*/, 13];
            case 2: return [4 /*yield*/, jobseeker_1.jobSeekerSchema.findById(mongoose.Types.ObjectId(body.jid)).exec()];
            case 3:
                jobseeker = _a.sent();
                if (!jobseeker) return [3 /*break*/, 12];
                flagList = void 0;
                if (!(body.fsts == 4)) return [3 /*break*/, 5];
                return [4 /*yield*/, flagdetails_1.flagDetailsSchema.find({ jid: mongoose.Types.ObjectId(body.jid) }).sort({ createdAt: -1 }).exec()];
            case 4:
                flagList = _a.sent();
                return [3 /*break*/, 7];
            case 5: return [4 /*yield*/, flagdetails_1.flagDetailsSchema.find({ jid: mongoose.Types.ObjectId(body.jid), fsts: body.fsts }).sort({ createdAt: -1 }).exec()];
            case 6:
                flagList = _a.sent();
                _a.label = 7;
            case 7:
                obj = {
                    jobSeekerDetails: jobseeker,
                    flagDetails: []
                };
                if (!(flagList.length > 0)) return [3 /*break*/, 11];
                d = 0;
                _a.label = 8;
            case 8:
                if (!(d < flagList.length)) return [3 /*break*/, 11];
                return [4 /*yield*/, attachment_1.attachmentSchema.find({ fid: flagList[d]._id }).exec()];
            case 9:
                attch = _a.sent();
                flagList[d]._doc.attachment = attch;
                obj.flagDetails = flagList;
                _a.label = 10;
            case 10:
                d++;
                return [3 /*break*/, 8];
            case 11:
                res.send(new status_1.Status(200, obj));
                return [3 /*break*/, 13];
            case 12:
                res.send(new APIError_1.APIError(201, errorMsg_1.jobSeekerNotExistsMsg));
                _a.label = 13;
            case 13: return [3 /*break*/, 15];
            case 14:
                error_3 = _a.sent();
                res.send(new APIError_1.APIError(201, error_3));
                return [3 /*break*/, 15];
            case 15: return [2 /*return*/];
        }
    });
}); });
router.post("/addFlag", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var newFlag, newFlagDetails, error_4;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 10, , 11]);
                body = req.body;
                if (!!body.cid) return [3 /*break*/, 1];
                res.send(new APIError_1.APIError(201, errorMsg_1.companyIdMsg));
                return [3 /*break*/, 9];
            case 1:
                if (!!body.rid) return [3 /*break*/, 2];
                res.send(new APIError_1.APIError(201, errorMsg_1.recuriterIdMsg));
                return [3 /*break*/, 9];
            case 2:
                if (!!body.jid) return [3 /*break*/, 3];
                res.send(new APIError_1.APIError(201, errorMsg_1.jobSeekerIdMsg));
                return [3 /*break*/, 9];
            case 3:
                if (!!body.cname.trim()) return [3 /*break*/, 4];
                res.send(new APIError_1.APIError(201, errorMsg_1.companyNameMsg));
                return [3 /*break*/, 9];
            case 4:
                if (!!body.fsts) return [3 /*break*/, 5];
                res.send(new APIError_1.APIError(201, errorMsg_1.fstsIdMsg));
                return [3 /*break*/, 9];
            case 5:
                if (!!body.whd.trim()) return [3 /*break*/, 6];
                res.send(new APIError_1.APIError(201, errorMsg_1.whtMsg));
                return [3 /*break*/, 9];
            case 6: return [4 /*yield*/, insertFlag(body)];
            case 7:
                newFlag = _a.sent();
                return [4 /*yield*/, newFlag.save()];
            case 8:
                newFlagDetails = _a.sent();
                if (newFlagDetails) {
                    //For Credits
                    insertOrUpdateCredits(body);
                    //For Credits
                    res.send(new status_1.Status(200, newFlagDetails));
                }
                else {
                    res.send(new APIError_1.APIError(201, errorMsg_1.errorInSaveMsg));
                }
                _a.label = 9;
            case 9: return [3 /*break*/, 11];
            case 10:
                error_4 = _a.sent();
                res.send(new APIError_1.APIError(201, error_4));
                return [3 /*break*/, 11];
            case 11: return [2 /*return*/];
        }
    });
}); });
router.post("/deleteJobSeeker", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var deleteJobSeeker, error_5;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 4, , 5]);
                body = req.body;
                if (!!body.jid) return [3 /*break*/, 1];
                res.send(new APIError_1.APIError(201, errorMsg_1.jobSeekerIdMsg));
                return [3 /*break*/, 3];
            case 1: return [4 /*yield*/, jobseeker_1.jobSeekerSchema.updateOne({ "_id": mongoose.Types.ObjectId(body.jid) }, { $set: { active: 0 } }).exec()];
            case 2:
                deleteJobSeeker = _a.sent();
                res.send(new status_1.Status(200, deleteJobSeeker));
                _a.label = 3;
            case 3: return [3 /*break*/, 5];
            case 4:
                error_5 = _a.sent();
                res.send(new APIError_1.APIError(201, error_5));
                return [3 /*break*/, 5];
            case 5: return [2 /*return*/];
        }
    });
}); });
router.post("/status", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var lookList, error_6;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, lookup_1.lookUpSchema.find({}).exec()];
            case 1:
                lookList = _a.sent();
                res.send(new status_1.Status(200, lookList));
                return [3 /*break*/, 3];
            case 2:
                error_6 = _a.sent();
                res.send(new APIError_1.APIError(201, error_6));
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
router.post("/flagAttachement", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var form;
    return __generator(this, function (_a) {
        form = new formidable.IncomingForm();
        form.parse(req, function (err, fields, files) {
            //  res.writeHead(200, { 'content-type': 'text/plain' });
            var body = fields;
            if (!body.cid) {
                res.send(new APIError_1.APIError(201, errorMsg_1.companyIdMsg));
            }
            else if (!body.rid) {
                res.send(new APIError_1.APIError(201, errorMsg_1.recuriterIdMsg));
            }
            else if (!body.jid) {
                res.send(new APIError_1.APIError(201, errorMsg_1.jobSeekerIdMsg));
            }
            else if (!body.fid) {
                res.send(new APIError_1.APIError(201, errorMsg_1.flagIdMsg));
            }
            else {
                var fList_1 = Object.values(files);
                var cnt_1 = 0;
                var newAchmnt_1;
                var _loop_1 = function (f) {
                    fs.readFile(fList_1[f].path, function (err, data) { return __awaiter(void 0, void 0, void 0, function () {
                        var urlPath, dbPath;
                        return __generator(this, function (_a) {
                            if (err) {
                                res.send(new APIError_1.APIError(201, err));
                            }
                            urlPath = './Attachment/' + "cid-" + body.cid + "/" + "rid-" + body.rid + "/" + "jid-" + body.jid + "/" + "fid-" + body.fid;
                            dbPath = '' + "cid-" + body.cid + "/" + "rid-" + body.rid + "/" + "jid-" + body.jid + "/" + "fid-" + body.fid + "/";
                            if (!fs.existsSync(urlPath)) {
                                try {
                                    fs.mkdirSync(urlPath, { recursive: true });
                                }
                                catch (error) {
                                    console.log(error.message);
                                }
                            }
                            fs.writeFile(urlPath + "/" + fList_1[f].name, data, function (err) { return __awaiter(void 0, void 0, void 0, function () {
                                var achmnt, error_7;
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0:
                                            if (!err) return [3 /*break*/, 1];
                                            console.log(err);
                                            res.send(new APIError_1.APIError(201, err));
                                            return [3 /*break*/, 4];
                                        case 1:
                                            _a.trys.push([1, 3, , 4]);
                                            achmnt = new attachment_1.attachmentSchema({
                                                cid: body.cid,
                                                rid: body.rid,
                                                jid: body.jid,
                                                fid: body.fid,
                                                path: dbPath + fList_1[f].name
                                            });
                                            return [4 /*yield*/, achmnt.save()];
                                        case 2:
                                            newAchmnt_1 = _a.sent();
                                            cnt_1++;
                                            if (cnt_1 == fList_1.length) {
                                                res.send(new status_1.Status(200, newAchmnt_1));
                                            }
                                            return [3 /*break*/, 4];
                                        case 3:
                                            error_7 = _a.sent();
                                            res.send(new APIError_1.APIError(201, error_7));
                                            return [3 /*break*/, 4];
                                        case 4: return [2 /*return*/];
                                    }
                                });
                            }); });
                            //Delete the file
                            fs.unlink(fList_1[f].path, function (err) {
                                if (err)
                                    res.send(new APIError_1.APIError(201, err));
                                // console.log('File deleted!');
                            });
                            return [2 /*return*/];
                        });
                    }); });
                };
                for (var f = 0; f < fList_1.length; f++) {
                    _loop_1(f);
                }
            }
        });
        return [2 /*return*/];
    });
}); });
module.exports = router;

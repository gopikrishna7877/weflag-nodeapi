"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var express_1 = require("express");
var mongoose = require("mongoose");
var status_1 = require("../utils/status");
var jobseeker_1 = require("../modals/jobseeker");
var flagdetails_1 = require("../modals/flagdetails");
var errorMsg_1 = require("../utils/errorMsg");
var APIError_1 = require("../utils/APIError");
var company_1 = require("../modals/company");
var recruiters_1 = require("../modals/recruiters");
var walletcredits_1 = require("../modals/walletcredits");
var creditslookup_1 = require("../modals/creditslookup");
var router = express_1.Router();
router.post("/chartsData1", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var body, monthNames, today, d, month, groupBarList, groupBarChart, i, fy, cM, cY, r, _loop_1, k, f, obj, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 4, , 5]);
                body = req.body;
                if (!!body.cid) return [3 /*break*/, 1];
                res.send(new APIError_1.APIError(201, errorMsg_1.companyIdMsg));
                return [3 /*break*/, 3];
            case 1:
                monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                today = new Date();
                groupBarList = [];
                return [4 /*yield*/, flagdetails_1.flagDetailsSchema.aggregate([
                        {
                            $project: {
                                fsts: 1,
                                cid: 1,
                                createdAt: 1,
                                _id: 0,
                                year: { $year: '$createdAt' },
                                month: { $month: '$createdAt' }
                            }
                        },
                        {
                            $match: {
                                cid: mongoose.Types.ObjectId(body.cid),
                                createdAt: {
                                    $gte: new Date(new Date().getFullYear(), new Date().getMonth() - 6),
                                    $lte: new Date(new Date().getFullYear(), new Date().getMonth() + 1)
                                }
                            }
                        },
                        {
                            $group: {
                                _id: {
                                    month: { $month: '$createdAt' },
                                    year: { $year: '$createdAt' },
                                    fsts: "$fsts",
                                    cid: "$cid"
                                },
                                monthCnt: { "$sum": 1 }
                            }
                        }
                    ])];
            case 2:
                groupBarChart = _a.sent();
                // setTimeout(() => {
                for (i = 6; i > 0; i--) {
                    fy = today.getFullYear();
                    cM = today.getMonth() + 1;
                    d = new Date(fy, cM - i, 1);
                    cY = d.getFullYear();
                    month = d.getMonth() + 1;
                    r = groupBarChart.filter(function (x) { return x._id.month == month; });
                    if (r.length > 0) {
                        _loop_1 = function (k) {
                            var filterFsts = r.filter(function (x) { return x._id.fsts == k; });
                            if (filterFsts.length > 0) {
                                groupBarList.push(filterFsts[0]);
                            }
                            else {
                                var obj = {
                                    _id: {
                                        month: month,
                                        "year": cY,
                                        fsts: k,
                                        "cid": body.cid
                                    },
                                    monthCnt: 0
                                };
                                groupBarList.push(obj);
                            }
                        };
                        for (k = 1; k < 4; k++) {
                            _loop_1(k);
                        }
                        // r.forEach(ele => {
                        //     groupBarList.push(ele)
                        // })
                    }
                    else {
                        for (f = 1; f < 4; f++) {
                            obj = {
                                _id: {
                                    month: month,
                                    "year": cY,
                                    fsts: f,
                                    "cid": body.cid
                                },
                                monthCnt: 0
                            };
                            groupBarList.push(obj);
                        }
                    }
                }
                res.send(new status_1.Status(200, groupBarList));
                _a.label = 3;
            case 3: return [3 /*break*/, 5];
            case 4:
                error_1 = _a.sent();
                res.send(new APIError_1.APIError(201, error_1));
                return [3 /*break*/, 5];
            case 5: return [2 /*return*/];
        }
    });
}); });
router.post("/chartsData", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var body, obj, pieChart, today, d, month, groupBarList, groupBarChart, i, fy, cM, cY, r, _loop_2, k_1, f, obj_1, lineChart, arr, name_1, fsts, objSeries, listObj_1, _loop_3, i_1, cddwclist, cddwc, _loop_4, i, error_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 7, , 8]);
                body = req.body;
                if (!!body.cid) return [3 /*break*/, 1];
                res.send(new APIError_1.APIError(201, errorMsg_1.companyIdMsg));
                return [3 /*break*/, 6];
            case 1:
                obj = {};
                return [4 /*yield*/, flagdetails_1.flagDetailsSchema.aggregate([
                        {
                            $project: {
                                fsts: 1,
                                cid: 1,
                                createdAt: 1,
                                _id: 0,
                                year: { $year: '$createdAt' },
                                month: { $month: '$createdAt' }
                            }
                        },
                        {
                            $match: {
                                cid: mongoose.Types.ObjectId(body.cid),
                                createdAt: {
                                    $gte: new Date(new Date().getFullYear(), new Date().getMonth()),
                                    $lte: new Date(new Date().getFullYear(), new Date().getMonth() + 1)
                                }
                            }
                        },
                        {
                            $group: {
                                _id: {
                                    month: { $month: '$createdAt' },
                                    year: { $year: '$createdAt' },
                                    fsts: "$fsts",
                                    cid: "$cid"
                                },
                                MonthCount: { "$sum": 1 }
                            }
                        },
                        {
                            $sort: { _id: 1 }
                        }
                    ])];
            case 2:
                pieChart = _a.sent();
                today = new Date();
                groupBarList = [];
                return [4 /*yield*/, flagdetails_1.flagDetailsSchema.aggregate([
                        {
                            $project: {
                                fsts: 1,
                                cid: 1,
                                createdAt: 1,
                                _id: 0,
                                year: { $year: '$createdAt' },
                                month: { $month: '$createdAt' }
                            }
                        },
                        {
                            $match: {
                                cid: mongoose.Types.ObjectId(body.cid),
                                createdAt: {
                                    $gte: new Date(new Date().getFullYear(), new Date().getMonth() - 6),
                                    $lte: new Date(new Date().getFullYear(), new Date().getMonth() + 1)
                                }
                            }
                        },
                        {
                            $group: {
                                _id: {
                                    month: { $month: '$createdAt' },
                                    year: { $year: '$createdAt' },
                                    fsts: "$fsts",
                                    cid: "$cid"
                                },
                                monthCnt: { "$sum": 1 }
                            }
                        }
                    ])];
            case 3:
                groupBarChart = _a.sent();
                for (i = 6; i > 0; i--) {
                    fy = today.getFullYear();
                    cM = today.getMonth() + 1;
                    d = new Date(fy, cM - i, 1);
                    cY = d.getFullYear();
                    month = d.getMonth() + 1;
                    r = groupBarChart.filter(function (x) { return x._id.month == month; });
                    if (r.length > 0) {
                        _loop_2 = function (k_1) {
                            var filterFsts = r.filter(function (x) { return x._id.fsts == k_1; });
                            if (filterFsts.length > 0) {
                                groupBarList.push(filterFsts[0]);
                            }
                            else {
                                var obj_2 = {
                                    _id: {
                                        month: month,
                                        "year": cY,
                                        fsts: k_1,
                                        "cid": body.cid
                                    },
                                    monthCnt: 0
                                };
                                groupBarList.push(obj_2);
                            }
                        };
                        for (k_1 = 1; k_1 < 4; k_1++) {
                            _loop_2(k_1);
                        }
                    }
                    else {
                        for (f = 1; f < 4; f++) {
                            obj_1 = {
                                _id: {
                                    month: month,
                                    "year": cY,
                                    "cid": body.cid,
                                    fsts: f
                                },
                                monthCnt: 0
                            };
                            groupBarList.push(obj_1);
                        }
                    }
                }
                return [4 /*yield*/, flagdetails_1.flagDetailsSchema.aggregate([
                        {
                            $project: {
                                fsts: 1,
                                cid: 1,
                                createdAt: 1,
                                _id: 0,
                                year: { $year: '$createdAt' },
                                month: { $month: '$createdAt' },
                                day: { $dayOfMonth: '$createdAt' }
                            }
                        },
                        {
                            $match: {
                                cid: mongoose.Types.ObjectId(body.cid),
                                createdAt: {
                                    $gte: new Date(new Date().getFullYear(), new Date().getMonth() - 6),
                                    $lte: new Date(new Date().getFullYear(), new Date().getMonth() + 1)
                                }
                            }
                        },
                        {
                            // $group: {
                            //     _id: {
                            //         "createdAt": {
                            //             $dateToString: {
                            //                 format: "%Y-%m-%d",
                            //                 date: "$createdAt"
                            //             }
                            //         },
                            //         fsts: "$fsts",
                            //         cid: "$cid",
                            //         year: { $year: '$createdAt' },
                            //         month: { $month: '$createdAt' },
                            //         day: { $dayOfMonth: '$createdAt' }
                            //     },
                            //     dayCount: { "$sum": 1 }
                            // }
                            $group: {
                                _id: {
                                    month: { $month: '$createdAt' },
                                    year: { $year: '$createdAt' },
                                    fsts: "$fsts",
                                    cid: "$cid"
                                },
                                monthCnt: { "$sum": 1 }
                            }
                        }
                    ])];
            case 4:
                lineChart = _a.sent();
                arr = [];
                fsts = void 0;
                objSeries = {};
                listObj_1 = [];
                _loop_3 = function (i_1) {
                    name_1 = '';
                    if (i_1 == 1) {
                        name_1 = "No Show";
                        fsts = 1;
                    }
                    else if (i_1 == 2) {
                        name_1 = "Not Joined";
                        fsts = 2;
                    }
                    else if (i_1 == 3) {
                        name_1 = "Joined";
                        fsts = 3;
                    }
                    objSeries["name"] = name_1;
                    objSeries["fsts"] = fsts;
                    var totalCnt = 0;
                    for (var k = 6; k > 0; k--) {
                        var fy = today.getFullYear();
                        var cM = today.getMonth() + 1;
                        d = new Date(fy, cM - k, 1);
                        var cY = d.getFullYear();
                        month = d.getMonth() + 1;
                        var r = lineChart.filter(function (x) { return x._id.fsts == i_1 && x._id.month == month; });
                        if (r.length > 0) {
                            r.forEach(function (ele) {
                                ele.name = getMonthValue(month) + " " + ele._id.year;
                                ele.value = ele.monthCnt;
                                totalCnt += ele.monthCnt,
                                    listObj_1.push(ele);
                            });
                        }
                        else {
                            var obj_3 = {
                                name: getMonthValue(month) + " " + cY,
                                value: 0
                            };
                            listObj_1.push(obj_3);
                        }
                    }
                    objSeries["totalCount"] = totalCnt;
                    objSeries["series"] = listObj_1;
                    arr.push(objSeries);
                    objSeries = {};
                    listObj_1 = [];
                };
                for (i_1 = 1; i_1 < 4; i_1++) {
                    _loop_3(i_1);
                }
                cddwclist = [];
                return [4 /*yield*/, walletcredits_1.transtionCreditsSchema.aggregate([
                        {
                            $match: {
                                createdAt: {
                                    $gte: new Date(new Date().getFullYear(), new Date().getMonth() - 5),
                                    $lte: new Date(new Date().getFullYear(), new Date().getMonth() + 1)
                                },
                                cid: mongoose.Types.ObjectId(body.cid)
                            }
                        },
                        {
                            $group: {
                                _id: {
                                    year: { $year: '$createdAt' },
                                    month: { $month: '$createdAt' },
                                    trtype: "$trtype",
                                    cid: "$cid"
                                },
                                count: { "$sum": "$credits" }
                            }
                        }
                    ])];
            case 5:
                cddwc = _a.sent();
                if (cddwc.length > 0) {
                    _loop_4 = function () {
                        var fy = today.getFullYear();
                        var cM = today.getMonth() + 1;
                        d = new Date(fy, cM - i, 1);
                        var cY = d.getFullYear();
                        month = d.getMonth() + 1;
                        var plus = 0;
                        var mins = 0;
                        var r = cddwc.filter(function (x) { return x._id.month == month; });
                        if (r.length > 0) {
                            r.forEach(function (ele) {
                                if (ele._id.trtype == "Search") {
                                    mins += ele.count;
                                }
                                else {
                                    plus += ele.count;
                                }
                                // ele.plusPoints = plus;
                                // ele.minsPoints = mins
                                // cddwclist.push(obj);
                            });
                            var obj_4 = {
                                cid: body.cid,
                                month: month,
                                plusPoints: plus,
                                minsPoints: mins,
                                year: cY
                            };
                            cddwclist.push(obj_4);
                        }
                        else {
                            var obj_5 = {
                                cid: body.cid,
                                month: month,
                                plusPoints: plus,
                                minsPoints: mins,
                                year: cY
                            };
                            cddwclist.push(obj_5);
                        }
                    };
                    for (i = 6; i > 0; i--) {
                        _loop_4();
                    }
                    // cddwc.forEach(ele1 => {
                    //     cddwclist.push({
                    //         cid: ele1._id.cid,
                    //         year: ele1._id.year,
                    //         month: ele1._id.month,
                    //         trtype: ele1._id.trtype,
                    //         count: ele1.count
                    //     })
                    // })
                    //res.send(new Status(200, cddwclist));
                }
                obj["pieChart"] = pieChart;
                obj["lineChart"] = arr;
                obj["groupBarChart"] = groupBarList;
                obj["stackBar100"] = cddwclist;
                res.send(new status_1.Status(200, obj));
                _a.label = 6;
            case 6: return [3 /*break*/, 8];
            case 7:
                error_2 = _a.sent();
                res.send(new APIError_1.APIError(201, error_2));
                return [3 /*break*/, 8];
            case 8: return [2 /*return*/];
        }
    });
}); });
router.post("/statusData", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var body, noshowCnt, notJoinedCnt, joinedCnt, obj, error_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 6, , 7]);
                body = req.body;
                if (!!body.cid) return [3 /*break*/, 1];
                res.send(new APIError_1.APIError(201, errorMsg_1.companyIdMsg));
                return [3 /*break*/, 5];
            case 1: return [4 /*yield*/, jobseeker_1.jobSeekerSchema.find({ "cid": mongoose.Types.ObjectId(body.cid), active: 1, fsts: 1 }).countDocuments()];
            case 2:
                noshowCnt = _a.sent();
                return [4 /*yield*/, jobseeker_1.jobSeekerSchema.find({ "cid": mongoose.Types.ObjectId(body.cid), active: 1, fsts: 2 }).countDocuments()];
            case 3:
                notJoinedCnt = _a.sent();
                return [4 /*yield*/, jobseeker_1.jobSeekerSchema.find({ "cid": mongoose.Types.ObjectId(body.cid), active: 1, fsts: 3 }).countDocuments()];
            case 4:
                joinedCnt = _a.sent();
                obj = {};
                obj["No Show"] = noshowCnt;
                obj["Not Joined"] = notJoinedCnt;
                obj["Joined"] = joinedCnt;
                res.send(new status_1.Status(200, obj));
                _a.label = 5;
            case 5: return [3 /*break*/, 7];
            case 6:
                error_3 = _a.sent();
                res.send(new APIError_1.APIError(201, error_3));
                return [3 /*break*/, 7];
            case 7: return [2 /*return*/];
        }
    });
}); });
router.post("/serachJobSeeker", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var body, key, jobseeker, error_4;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 5, , 6]);
                body = req.body;
                if (!!body.value) return [3 /*break*/, 1];
                res.send(new APIError_1.APIError(201, errorMsg_1.serachTextMsg));
                return [3 /*break*/, 4];
            case 1:
                if (!(body.value.length < 3)) return [3 /*break*/, 2];
                res.send(new APIError_1.APIError(201, errorMsg_1.maxThreeChr));
                return [3 /*break*/, 4];
            case 2:
                key = eval("/.*" + body.value + "+.*/i");
                return [4 /*yield*/, jobseeker_1.jobSeekerSchema.find({ $or: [{ jsname: key }, { jsemail: key }, { jscon: key }, { jsaorp: key }], active: 1 }).exec()];
            case 3:
                jobseeker = _a.sent();
                res.send(new status_1.Status(200, jobseeker));
                _a.label = 4;
            case 4: return [3 /*break*/, 6];
            case 5:
                error_4 = _a.sent();
                res.send(new APIError_1.APIError(201, error_4));
                return [3 /*break*/, 6];
            case 6: return [2 /*return*/];
        }
    });
}); });
router.post("/_serachJobSeeker", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var body, company, recruiter, key, jobseeker, newCredits, uCompany, crTrn, newTrn, error_5;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 15, , 16]);
                body = req.body;
                if (!!body.value) return [3 /*break*/, 1];
                res.send(new APIError_1.APIError(201, errorMsg_1.serachTextMsg));
                return [3 /*break*/, 14];
            case 1:
                if (!(body.value.length < 3)) return [3 /*break*/, 2];
                res.send(new APIError_1.APIError(201, errorMsg_1.maxThreeChr));
                return [3 /*break*/, 14];
            case 2: return [4 /*yield*/, company_1.companySchema.findOne({ _id: mongoose.Types.ObjectId(body.cid) }).exec()];
            case 3:
                company = _a.sent();
                if (!company) return [3 /*break*/, 13];
                return [4 /*yield*/, recruiters_1.recuriterSchema.findOne({ _id: mongoose.Types.ObjectId(body.rid) }).exec()];
            case 4:
                recruiter = _a.sent();
                if (!recruiter) return [3 /*break*/, 11];
                if (!(company.credits > 0)) return [3 /*break*/, 9];
                key = eval("/.*" + body.value + "+.*/i");
                return [4 /*yield*/, jobseeker_1.jobSeekerSchema.find({ $or: [{ jsname: key }, { jsemail: key }, { jscon: key }, { jsaorp: key }], active: 1 }).exec()];
            case 5:
                jobseeker = _a.sent();
                if (!(jobseeker.length > 0)) return [3 /*break*/, 7];
                newCredits = company.credits;
                newCredits = newCredits + errorMsg_1.serachCredits;
                return [4 /*yield*/, company_1.companySchema.updateOne({
                        _id: mongoose.Types.ObjectId(body.cid)
                    }, {
                        $set: {
                            credits: newCredits
                        }
                    })];
            case 6:
                uCompany = _a.sent();
                crTrn = new walletcredits_1.transtionCreditsSchema({
                    cid: body.cid,
                    rid: body.rid,
                    credits: errorMsg_1.serachCredits,
                    rname: recruiter.rname,
                    rcon: recruiter.con,
                    trtype: errorMsg_1.searchCrType
                });
                newTrn = crTrn.save();
                res.send(new status_1.Status(200, jobseeker));
                return [3 /*break*/, 8];
            case 7:
                res.send(new status_1.Status(200, jobseeker));
                _a.label = 8;
            case 8: return [3 /*break*/, 10];
            case 9:
                res.send(new APIError_1.APIError(201, errorMsg_1.creditBLMsg));
                _a.label = 10;
            case 10: return [3 /*break*/, 12];
            case 11:
                res.send(new APIError_1.APIError(201, errorMsg_1.recuriterNotExistMsg));
                _a.label = 12;
            case 12: return [3 /*break*/, 14];
            case 13:
                res.send(new APIError_1.APIError(201, errorMsg_1.companyNotExistsMsg));
                _a.label = 14;
            case 14: return [3 /*break*/, 16];
            case 15:
                error_5 = _a.sent();
                res.send(new APIError_1.APIError(201, error_5));
                return [3 /*break*/, 16];
            case 16: return [2 /*return*/];
        }
    });
}); });
router.post("/getCompanyDetails", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var body, company, recu, error_6;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 6, , 7]);
                body = req.body;
                if (!!body.cid) return [3 /*break*/, 1];
                res.send(new APIError_1.APIError(201, errorMsg_1.companyIdMsg));
                return [3 /*break*/, 5];
            case 1:
                if (!!body.rid) return [3 /*break*/, 2];
                res.send(new APIError_1.APIError(201, errorMsg_1.recuriterIdMsg));
                return [3 /*break*/, 5];
            case 2: return [4 /*yield*/, company_1.companySchema.findOne({ _id: mongoose.Types.ObjectId(body.cid) }).exec()];
            case 3:
                company = _a.sent();
                return [4 /*yield*/, recruiters_1.recuriterSchema.findOne({ _id: mongoose.Types.ObjectId(body.rid) }).exec()];
            case 4:
                recu = _a.sent();
                company._doc.ractive = recu.active;
                res.send(new status_1.Status(200, company));
                _a.label = 5;
            case 5: return [3 /*break*/, 7];
            case 6:
                error_6 = _a.sent();
                res.send(new APIError_1.APIError(201, error_6));
                return [3 /*break*/, 7];
            case 7: return [2 /*return*/];
        }
    });
}); });
router.post("/creditsUpdating", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var body, company, crL, searchP, recruiter, jobseeker, newCredits, uCompany, crTrn, newTrn, error_7;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 15, , 16]);
                body = req.body;
                if (!!body.cid) return [3 /*break*/, 1];
                res.send(new APIError_1.APIError(201, errorMsg_1.companyIdMsg));
                return [3 /*break*/, 14];
            case 1:
                if (!!body.rid) return [3 /*break*/, 2];
                res.send(new APIError_1.APIError(201, errorMsg_1.recuriterIdMsg));
                return [3 /*break*/, 14];
            case 2:
                if (!!body.jid) return [3 /*break*/, 3];
                res.send(new APIError_1.APIError(201, errorMsg_1.recuriterIdMsg));
                return [3 /*break*/, 14];
            case 3: return [4 /*yield*/, company_1.companySchema.findOne({ _id: mongoose.Types.ObjectId(body.cid) }).exec()];
            case 4:
                company = _a.sent();
                if (!company) return [3 /*break*/, 14];
                return [4 /*yield*/, creditslookup_1.crLookSchema.find({}).exec()];
            case 5:
                crL = _a.sent();
                searchP = Number(crL[0].search.toString().replace('-', '').trim());
                if (!(company.credits >= searchP)) return [3 /*break*/, 13];
                return [4 /*yield*/, recruiters_1.recuriterSchema.findOne({ _id: mongoose.Types.ObjectId(body.rid) }).exec()];
            case 6:
                recruiter = _a.sent();
                if (!recruiter) return [3 /*break*/, 11];
                return [4 /*yield*/, jobseeker_1.jobSeekerSchema.findOne({ _id: mongoose.Types.ObjectId(body.jid) }).exec()];
            case 7:
                jobseeker = _a.sent();
                if (!jobseeker) return [3 /*break*/, 9];
                newCredits = company.credits + crL[0].search;
                return [4 /*yield*/, company_1.companySchema.updateOne({
                        _id: mongoose.Types.ObjectId(body.cid)
                    }, {
                        $set: {
                            credits: newCredits
                        }
                    })];
            case 8:
                uCompany = _a.sent();
                crTrn = new walletcredits_1.transtionCreditsSchema({
                    cid: body.cid,
                    rid: body.rid,
                    jid: body.jid,
                    credits: crL[0].search,
                    rname: recruiter.rname,
                    rcon: recruiter.con,
                    jsname: jobseeker.jsname,
                    trtype: errorMsg_1.searchCrType
                });
                newTrn = crTrn.save();
                res.send(new status_1.Status(200, company));
                return [3 /*break*/, 10];
            case 9:
                res.send(new APIError_1.APIError(201, errorMsg_1.jobSeekerNotExistsMsg));
                _a.label = 10;
            case 10: return [3 /*break*/, 12];
            case 11:
                res.send(new APIError_1.APIError(201, errorMsg_1.recuriterNotExistMsg));
                _a.label = 12;
            case 12: return [3 /*break*/, 14];
            case 13:
                res.send(new APIError_1.APIError(201, errorMsg_1.creditBLMsg));
                _a.label = 14;
            case 14: return [3 /*break*/, 16];
            case 15:
                error_7 = _a.sent();
                res.send(new APIError_1.APIError(201, error_7));
                return [3 /*break*/, 16];
            case 16: return [2 /*return*/];
        }
    });
}); });
function getMonthValue(val) {
    switch (val) {
        case 1:
            return 'Jan';
        case 2:
            return "Feb";
        case 3:
            return "Mar";
        case 4:
            return "Apr";
        case 5:
            return "May";
        case 6:
            return "Jun";
        case 7:
            return "Jul";
        case 8:
            return "Aug";
        case 9:
            return "Sep";
        case 10:
            return "Oct";
        case 11:
            return "Nov";
        case 12:
            return "Dec";
    }
}
router.post("/crtrndetailsDaywisecount", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var cddwc, cddwclist_1, error_8;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, walletcredits_1.transtionCreditsSchema.aggregate([
                        {
                            $project: {
                                trtype: 1, cid: 1, createdAt: 1, _id: 0,
                                year: { $year: '$createdAt' }, month: { $month: '$createdAt' }
                            }
                        },
                        {
                            $match: {
                                createdAt: {
                                    $gte: new Date(new Date().getFullYear(), new Date().getMonth() - 5),
                                    $lte: new Date(new Date().getFullYear(), new Date().getMonth() + 1)
                                }
                            }
                        },
                        {
                            $group: {
                                _id: {
                                    year: { $year: '$createdAt' },
                                    month: { $month: '$createdAt' },
                                    trtype: "$trtype",
                                    cid: "$cid"
                                },
                                "count": { "$sum": 1 }
                            }
                        }
                    ])];
            case 1:
                cddwc = _a.sent();
                cddwclist_1 = [];
                if (cddwc) {
                    cddwc.forEach(function (ele1) {
                        cddwclist_1.push({
                            cid: ele1._id.cid,
                            year: ele1._id.year,
                            month: getMonthValue(ele1._id.month),
                            trtype: ele1._id.trtype,
                            count: ele1.count
                        });
                    });
                    //res.send(new Status(200, cddwclist));
                }
                else {
                    res.send(new status_1.Status(200, errorMsg_1.noDataMsg));
                }
                return [3 /*break*/, 3];
            case 2:
                error_8 = _a.sent();
                res.send(new APIError_1.APIError(201, error_8));
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
module.exports = router;

import { Router } from 'express';
import * as mongoose from 'mongoose';

import { Status } from '../utils/status';
import { jobSeekerSchema } from '../modals/jobseeker';
import { flagDetailsSchema } from '../modals/flagdetails';
import { companyIdMsg, serachTextMsg, maxThreeChr, serachCredits, companyNotExistsMsg, creditBLMsg, recuriterIdMsg, recuriterNotExistMsg, searchCrType, jobSeekerNotExistsMsg, noDataMsg } from '../utils/errorMsg';
import { APIError } from '../utils/APIError';
import { companySchema } from '../modals/company';
import { recuriterSchema } from '../modals/recruiters';
import { transtionCreditsSchema } from '../modals/walletcredits';
import { crLookSchema } from '../modals/creditslookup';
let router = Router();

router.post("/chartsData1", async (req, res, next) => {
    try {
        let body = req.body;
        if (!body.cid) {
            res.send(new APIError(201, companyIdMsg));
        }
        else {
            var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            var today = new Date();
            var d;
            var month;
            var groupBarList = [];
            let groupBarChart = await flagDetailsSchema.aggregate([
                {
                    $project: {
                        fsts: 1,
                        cid: 1,
                        createdAt: 1,
                        _id: 0,
                        year: { $year: '$createdAt' },
                        month: { $month: '$createdAt' }
                    }
                },
                {
                    $match: {
                        cid: mongoose.Types.ObjectId(body.cid),
                        createdAt: {
                            $gte: new Date(new Date().getFullYear(), new Date().getMonth() - 6),
                            $lte: new Date(new Date().getFullYear(), new Date().getMonth() + 1)
                        }
                    }
                },
                {
                    $group: {
                        _id: {
                            month: { $month: '$createdAt' },
                            year: { $year: '$createdAt' },
                            fsts: "$fsts",
                            cid: "$cid"
                        },
                        monthCnt: { "$sum": 1 }
                    }
                }
            ]);

            // setTimeout(() => {
            for (var i = 6; i > 0; i--) {
                let fy = today.getFullYear();
                let cM = today.getMonth() + 1;
                d = new Date(fy, cM - i, 1);
                let cY = d.getFullYear();
                month = d.getMonth() + 1;
                let r = groupBarChart.filter(x => x._id.month == month);
                if (r.length > 0) {
                    for (let k = 1; k < 4; k++) {
                        let filterFsts = r.filter(x => x._id.fsts == k);
                        if (filterFsts.length > 0) {
                            groupBarList.push(filterFsts[0])
                        }
                        else {
                            let obj = {
                                _id: {
                                    month: month,
                                    "year": cY,
                                    fsts: k,
                                    "cid": body.cid
                                },
                                monthCnt: 0
                            }
                            groupBarList.push(obj)
                        }


                    }
                    // r.forEach(ele => {
                    //     groupBarList.push(ele)
                    // })

                }
                else {
                    for (let f = 1; f < 4; f++) {
                        let obj = {
                            _id: {
                                month: month,
                                "year": cY,
                                fsts: f,
                                "cid": body.cid
                            },
                            monthCnt: 0
                        }
                        groupBarList.push(obj)

                    }

                }
            }
            res.send(new Status(200, groupBarList));
            //}, 1000);


        }
    }
    catch (error) {
        res.send(new APIError(201, error));
    }
})

router.post("/chartsData", async (req, res, next) => {
    try {
        let body = req.body;
        if (!body.cid) {
            res.send(new APIError(201, companyIdMsg));
        }
        else {
            let obj = {};
            let pieChart = await flagDetailsSchema.aggregate([
                {
                    $project: {
                        fsts: 1,
                        cid: 1,
                        createdAt: 1,
                        _id: 0,
                        year: { $year: '$createdAt' },
                        month: { $month: '$createdAt' }
                    }
                },
                {
                    $match: {
                        cid: mongoose.Types.ObjectId(body.cid),
                        createdAt: {
                            $gte: new Date(new Date().getFullYear(), new Date().getMonth()),
                            $lte: new Date(new Date().getFullYear(), new Date().getMonth() + 1)
                        }
                    }
                },
                {
                    $group:
                    {
                        _id: {
                            month: { $month: '$createdAt' },
                            year: { $year: '$createdAt' },
                            fsts: "$fsts",
                            cid: "$cid"
                        },
                        MonthCount: { "$sum": 1 }
                    }
                },
                {
                    $sort: { _id: 1 }
                }
            ]);
            // let groupBarChart = await flagDetailsSchema.aggregate([
            //     {
            //         $project: {
            //             fsts: 1,
            //             cid: 1,
            //             createdAt: 1,
            //             _id: 0,
            //             year: { $year: '$createdAt' },
            //             month: { $month: '$createdAt' }
            //         }
            //     },
            //     {
            //         $match: {
            //             cid: mongoose.Types.ObjectId(body.cid),
            //             createdAt: {
            //                 $gte: new Date(new Date().getFullYear(), new Date().getMonth() - 6),
            //                 $lte: new Date(new Date().getFullYear(), new Date().getMonth() + 1)
            //             }
            //         }
            //     },
            //     {
            //         $group: {
            //             _id: {
            //                 month: { $month: '$createdAt' },
            //                 year: { $year: '$createdAt' },
            //                 fsts: "$fsts",
            //                 cid: "$cid"
            //             },
            //             monthCnt: { "$sum": 1 }
            //         }
            //     }
            // ]);

            var today = new Date();
            var d;
            var month;
            var groupBarList = [];
            let groupBarChart = await flagDetailsSchema.aggregate([
                {
                    $project: {
                        fsts: 1,
                        cid: 1,
                        createdAt: 1,
                        _id: 0,
                        year: { $year: '$createdAt' },
                        month: { $month: '$createdAt' }
                    }
                },
                {
                    $match: {
                        cid: mongoose.Types.ObjectId(body.cid),
                        createdAt: {
                            $gte: new Date(new Date().getFullYear(), new Date().getMonth() - 6),
                            $lte: new Date(new Date().getFullYear(), new Date().getMonth() + 1)
                        }
                    }
                },
                {
                    $group: {
                        _id: {
                            month: { $month: '$createdAt' },
                            year: { $year: '$createdAt' },
                            fsts: "$fsts",
                            cid: "$cid"
                        },
                        monthCnt: { "$sum": 1 }
                    }
                }
            ]);

            for (var i = 6; i > 0; i--) {
                let fy = today.getFullYear();
                let cM = today.getMonth() + 1;
                d = new Date(fy, cM - i, 1);
                let cY = d.getFullYear();
                month = d.getMonth() + 1;
                let r = groupBarChart.filter(x => x._id.month == month);
                if (r.length > 0) {
                    for (let k = 1; k < 4; k++) {
                        let filterFsts = r.filter(x => x._id.fsts == k);
                        if (filterFsts.length > 0) {
                            groupBarList.push(filterFsts[0])
                        }
                        else {
                            let obj = {
                                _id: {
                                    month: month,
                                    "year": cY,
                                    fsts: k,
                                    "cid": body.cid
                                },
                                monthCnt: 0
                            }
                            groupBarList.push(obj)
                        }


                    }

                }
                else {
                    for (let f = 1; f < 4; f++) {
                        let obj = {
                            _id: {
                                month: month,
                                "year": cY,
                                "cid": body.cid,
                                fsts: f
                            },
                            monthCnt: 0
                        }
                        groupBarList.push(obj)

                    }

                }
            }


            let lineChart = await flagDetailsSchema.aggregate([
                {
                    $project: {
                        fsts: 1,
                        cid: 1,
                        createdAt: 1,
                        _id: 0,
                        year: { $year: '$createdAt' },
                        month: { $month: '$createdAt' },
                        day: { $dayOfMonth: '$createdAt' }
                    }
                },
                {
                    $match: {
                        cid: mongoose.Types.ObjectId(body.cid),
                        createdAt: {
                            $gte: new Date(new Date().getFullYear(), new Date().getMonth() - 6),
                            $lte: new Date(new Date().getFullYear(), new Date().getMonth() + 1)
                        }
                    }
                },
                {
                    // $group: {
                    //     _id: {
                    //         "createdAt": {
                    //             $dateToString: {
                    //                 format: "%Y-%m-%d",
                    //                 date: "$createdAt"
                    //             }
                    //         },
                    //         fsts: "$fsts",
                    //         cid: "$cid",
                    //         year: { $year: '$createdAt' },
                    //         month: { $month: '$createdAt' },
                    //         day: { $dayOfMonth: '$createdAt' }
                    //     },
                    //     dayCount: { "$sum": 1 }
                    // }
                    $group: {
                        _id: {
                            month: { $month: '$createdAt' },
                            year: { $year: '$createdAt' },
                            fsts: "$fsts",
                            cid: "$cid"
                        },
                        monthCnt: { "$sum": 1 }
                    }
                }
            ]);

            let arr = [];
            let name;
            let fsts;
            let objSeries = {};
            let listObj = []
            for (let i = 1; i < 4; i++) {
                name = '';
                if (i == 1) {
                    name = "No Show";
                    fsts = 1
                }
                else if (i == 2) {
                    name = "Not Joined";
                    fsts = 2
                }
                else if (i == 3) {
                    name = "Joined";
                    fsts = 3
                }
                objSeries["name"] = name;
                objSeries["fsts"] = fsts;
                let totalCnt = 0;

                for (var k = 6; k > 0; k--) {
                    let fy = today.getFullYear();
                    let cM = today.getMonth() + 1;
                    d = new Date(fy, cM - k, 1);
                    let cY = d.getFullYear();
                    month = d.getMonth() + 1;
                    let r = lineChart.filter(x => x._id.fsts == i && x._id.month == month);
                    if (r.length > 0) {
                        r.forEach(ele => {
                            ele.name = getMonthValue(month) + " " + ele._id.year;
                            ele.value = ele.monthCnt
                            totalCnt += ele.monthCnt,
                                listObj.push(ele)
                        });
                    }
                    else {
                        let obj = {
                            name: getMonthValue(month) + " " + cY,
                            value: 0
                        }
                        listObj.push(obj)
                    }
                }
                objSeries["totalCount"] = totalCnt;
                objSeries["series"] = listObj;
                arr.push(objSeries);
                objSeries = {};
                listObj = [];
            }

            // let arr = [];
            // let name;
            // let color;
            // let totalCnt;
            // let fsts;
            // let objSeries = {};
            // let listObj = []
            // for (let i = 1; i < 4; i++) {
            //     name = '';
            //     if (i == 1) {
            //         name = "No Show";
            //         color = "#ff9800"
            //         fsts = 1
            //     }
            //     else if (i == 2) {
            //         name = "Not Joined";
            //         color = "#f44336";
            //         fsts = 2
            //     }
            //     else if (i == 3) {
            //         name = "Joined";
            //         color = "#4caf50";
            //         fsts = 3
            //     }
            //     objSeries["name"] = name;
            //     objSeries["color"] = color;
            //     objSeries["fsts"] = fsts;
            //     let totalCnt = 0;
            //     for (let j = 1; j <= new Date().getDate(); j++) {
            //         let list: any = lineChart.filter(x => x._id.day == j && x._id.fsts == i);
            //         if (list.length == 0) {
            //             list = {
            //                 value: 0,
            //                 name: j,
            //                 month: new Date().getMonth() + 1
            //             }
            //             listObj.push(list);
            //         }
            //         else {
            //             list.forEach(ele => {
            //                 let obj = {};
            //                 obj["value"] = ele.dayCount;
            //                 obj["name"] = j;
            //                 obj["month"] = ele._id.month;
            //                 totalCnt += ele.dayCount
            //                 listObj.push(obj)
            //             });
            //         }

            //     }
            //     objSeries["totalCount"] = totalCnt;
            //     objSeries["series"] = listObj;
            //     arr.push(objSeries);
            //     objSeries = {};
            //     listObj = [];
            // }
            let cddwclist: any = [];
            // let cddwc = await transtionCreditsSchema.aggregate([
            //     {
            //         $project: {
            //             trtype: 1, cid: 1, createdAt: 1, _id: 0,
            //             year: { $year: '$createdAt' }, month: { $month: '$createdAt' }
            //         }
            //     },
            //     {
            //         $match: {
            //             cid: mongoose.Types.ObjectId(body.cid),
            //             createdAt: {
            //                 $gte: new Date(new Date().getFullYear(), new Date().getMonth() - 5),
            //                 $lte: new Date(new Date().getFullYear(), new Date().getMonth() + 1)
            //             }
            //         }
            //     },
            //     { $group: { _id: { year: { $year: '$createdAt' }, month: { $month: '$createdAt' }, trtype: "$trtype", cid: "$cid" }, "count": { "$sum": 1 } } }
            // ]);
            let cddwc = await transtionCreditsSchema.aggregate([
                {
                    $match: {
                        createdAt: {
                            $gte: new Date(new Date().getFullYear(), new Date().getMonth() - 5),
                            $lte: new Date(new Date().getFullYear(), new Date().getMonth() + 1)
                        },
                        cid: mongoose.Types.ObjectId(body.cid),
                    }
                },
                {
                    $group: {
                        _id: {
                            year: { $year: '$createdAt' },
                            month: { $month: '$createdAt' },
                            trtype: "$trtype",
                            cid: "$cid"
                        },
                        count: { "$sum": "$credits" }
                    }
                }
            ]);



            if (cddwc.length > 0) {
                for (var i = 6; i > 0; i--) {
                    let fy = today.getFullYear();
                    let cM = today.getMonth() + 1;
                    d = new Date(fy, cM - i, 1);
                    let cY = d.getFullYear();
                    month = d.getMonth() + 1;
                    let plus = 0;
                    let mins = 0;
                    let r = cddwc.filter(x => x._id.month == month);
                    if (r.length > 0) {
                        r.forEach(ele => {
                            if (ele._id.trtype == "Search") {
                                mins += ele.count
                            }
                            else {
                                plus += ele.count
                            }
                            // ele.plusPoints = plus;
                            // ele.minsPoints = mins
                            // cddwclist.push(obj);
                        });
                        let obj = {
                            cid: body.cid,
                            month: month,
                            plusPoints: plus,
                            minsPoints: mins,
                            year: cY
                        }
                        cddwclist.push(obj);

                    }
                    else {
                        let obj = {
                            cid: body.cid,
                            month: month,
                            plusPoints: plus,
                            minsPoints: mins,
                            year: cY
                        }
                        cddwclist.push(obj);
                    }
                }
                // cddwc.forEach(ele1 => {
                //     cddwclist.push({
                //         cid: ele1._id.cid,
                //         year: ele1._id.year,
                //         month: ele1._id.month,
                //         trtype: ele1._id.trtype,
                //         count: ele1.count
                //     })
                // })

                //res.send(new Status(200, cddwclist));
            }

            obj["pieChart"] = pieChart;
            obj["lineChart"] = arr;
            obj["groupBarChart"] = groupBarList;
            obj["stackBar100"] = cddwclist;
            res.send(new Status(200, obj));
        }

    } catch (error) {
        res.send(new APIError(201, error));
    }
})



router.post("/statusData", async (req, res, next) => {
    try {
        let body = req.body;
        if (!body.cid) {
            res.send(new APIError(201, companyIdMsg));
        }
        else {
            let noshowCnt = await jobSeekerSchema.find({ "cid": mongoose.Types.ObjectId(body.cid), active: 1, fsts: 1 }).countDocuments();
            let notJoinedCnt = await jobSeekerSchema.find({ "cid": mongoose.Types.ObjectId(body.cid), active: 1, fsts: 2 }).countDocuments();
            let joinedCnt = await jobSeekerSchema.find({ "cid": mongoose.Types.ObjectId(body.cid), active: 1, fsts: 3 }).countDocuments();
            let obj = {};
            obj["No Show"] = noshowCnt;
            obj["Not Joined"] = notJoinedCnt;
            obj["Joined"] = joinedCnt;
            res.send(new Status(200, obj));
        }

    } catch (error) {
        res.send(new APIError(201, error));
    }
});
router.post("/serachJobSeeker", async (req, res, next) => {
    try {
        let body = req.body;
        if (!body.value) {
            res.send(new APIError(201, serachTextMsg));
        }
        else if (body.value.length < 3) {
            res.send(new APIError(201, maxThreeChr));
        }
        else {
            let key = eval(`/.*${body.value}+.*/i`);
            let jobseeker = await jobSeekerSchema.find({ $or: [{ jsname: key }, { jsemail: key }, { jscon: key }, { jsaorp: key }], active: 1 }).exec();
            res.send(new Status(200, jobseeker));
        }

    } catch (error) {
        res.send(new APIError(201, error));
    }
});

router.post("/_serachJobSeeker", async (req, res, next) => {
    try {
        let body = req.body;
        if (!body.value) {
            res.send(new APIError(201, serachTextMsg));
        }
        else if (body.value.length < 3) {
            res.send(new APIError(201, maxThreeChr));
        }
        // else if (!body.cid) {
        //     res.send(new APIError(201, companyIdMsg));
        // }
        // else if (!body.rid) {
        //     res.send(new APIError(201, recuriterIdMsg));
        // }
        else {
            let company: any = await companySchema.findOne({ _id: mongoose.Types.ObjectId(body.cid) }).exec();
            if (company) {
                let recruiter: any = await recuriterSchema.findOne({ _id: mongoose.Types.ObjectId(body.rid) }).exec();
                if (recruiter) {
                    if (company.credits > 0) {
                        let key = eval(`/.*${body.value}+.*/i`);
                        let jobseeker = await jobSeekerSchema.find({ $or: [{ jsname: key }, { jsemail: key }, { jscon: key }, { jsaorp: key }], active: 1 }).exec();
                        if (jobseeker.length > 0) {
                            let newCredits = company.credits;
                            newCredits = newCredits + serachCredits;
                            let uCompany = await companySchema.updateOne(
                                {
                                    _id: mongoose.Types.ObjectId(body.cid)
                                },
                                {
                                    $set: {
                                        credits: newCredits
                                    }
                                }
                            );
                            let crTrn = new transtionCreditsSchema({
                                cid: body.cid,
                                rid: body.rid,
                                credits: serachCredits,
                                rname: recruiter.rname,
                                rcon: recruiter.con,
                                trtype: searchCrType
                            })
                            let newTrn = crTrn.save();
                            res.send(new Status(200, jobseeker));
                        }
                        else {
                            res.send(new Status(200, jobseeker));
                        }

                    }
                    else {
                        res.send(new APIError(201, creditBLMsg));
                    }
                }
                else {
                    res.send(new APIError(201, recuriterNotExistMsg));
                }
            }
            else {
                res.send(new APIError(201, companyNotExistsMsg));
            }
        }

    } catch (error) {
        res.send(new APIError(201, error));
    }
});

router.post("/getCompanyDetails", async (req, res, next) => {
    try {
        let body = req.body;
        if (!body.cid) {
            res.send(new APIError(201, companyIdMsg))
        }
        else if (!body.rid) {
            res.send(new APIError(201, recuriterIdMsg))
        }
        else {
            let company: any = await companySchema.findOne({ _id: mongoose.Types.ObjectId(body.cid) }).exec();
            let recu: any = await recuriterSchema.findOne({ _id: mongoose.Types.ObjectId(body.rid) }).exec();
            company._doc.ractive = recu.active;
            res.send(new Status(200, company));
        }
    } catch (error) {
        res.send(new APIError(201, error));

    }
})

router.post("/creditsUpdating", async (req, res, next) => {
    try {
        let body = req.body;
        if (!body.cid) {
            res.send(new APIError(201, companyIdMsg));
        }
        else if (!body.rid) {
            res.send(new APIError(201, recuriterIdMsg));
        }
        else if (!body.jid) {
            res.send(new APIError(201, recuriterIdMsg));
        }
        else {
            let company: any = await companySchema.findOne({ _id: mongoose.Types.ObjectId(body.cid) }).exec();
            if (company) {
                let crL: any = await crLookSchema.find({}).exec();
                let searchP = Number(crL[0].search.toString().replace('-', '').trim())
                if (company.credits >= searchP) {
                    let recruiter: any = await recuriterSchema.findOne({ _id: mongoose.Types.ObjectId(body.rid) }).exec();
                    if (recruiter) {
                        let jobseeker: any = await jobSeekerSchema.findOne({ _id: mongoose.Types.ObjectId(body.jid) }).exec();
                        if (jobseeker) {
                            //let crL = await crLookSchema.find({}).exec();
                            let newCredits = company.credits + crL[0].search;
                            let uCompany = await companySchema.updateOne(
                                {
                                    _id: mongoose.Types.ObjectId(body.cid)
                                },
                                {
                                    $set: {
                                        credits: newCredits
                                    }
                                }
                            );
                            let crTrn = new transtionCreditsSchema({
                                cid: body.cid,
                                rid: body.rid,
                                jid: body.jid,
                                credits: crL[0].search,
                                rname: recruiter.rname,
                                rcon: recruiter.con,
                                jsname: jobseeker.jsname,
                                trtype: searchCrType
                            })
                            let newTrn = crTrn.save();
                            res.send(new Status(200, company));
                        }
                        else {
                            res.send(new APIError(201, jobSeekerNotExistsMsg));
                        }
                    }
                    else {
                        res.send(new APIError(201, recuriterNotExistMsg));
                    }
                }
                else {
                    res.send(new APIError(201, creditBLMsg));
                }
            }
        }
    } catch (error) {
        res.send(new APIError(201, error))
    }
})

function getMonthValue(val) {
    switch (val) {
        case 1:
            return 'Jan';
        case 2:
            return "Feb";
        case 3:
            return "Mar";
        case 4:
            return "Apr";
        case 5:
            return "May";
        case 6:
            return "Jun";
        case 7:
            return "Jul";
        case 8:
            return "Aug";
        case 9:
            return "Sep";
        case 10:
            return "Oct";
        case 11:
            return "Nov";
        case 12:
            return "Dec";
    }
}

router.post("/crtrndetailsDaywisecount", async (req, res) => {
    try {
        let cddwc = await transtionCreditsSchema.aggregate([
            {
                $project: {
                    trtype: 1, cid: 1, createdAt: 1, _id: 0,
                    year: { $year: '$createdAt' }, month: { $month: '$createdAt' }
                }
            },
            {
                $match: {
                    createdAt: {
                        $gte: new Date(new Date().getFullYear(), new Date().getMonth() - 5),
                        $lte: new Date(new Date().getFullYear(), new Date().getMonth() + 1)
                    }
                }
            },
            {
                $group: {
                    _id: {
                        year: { $year: '$createdAt' },
                        month: { $month: '$createdAt' },
                        trtype: "$trtype",
                        cid: "$cid"
                    },
                    "count": { "$sum": 1 }
                }
            }
        ]);

        let cddwclist: any = [];

        if (cddwc) {
            cddwc.forEach(ele1 => {
                cddwclist.push({
                    cid: ele1._id.cid,
                    year: ele1._id.year,
                    month: getMonthValue(ele1._id.month),
                    trtype: ele1._id.trtype,
                    count: ele1.count
                })
            })

            //res.send(new Status(200, cddwclist));
        }
        else {
            res.send(new Status(200, noDataMsg));
        }
    }
    catch (error) {
        res.send(new APIError(201, error));
    }
})

export =router; 
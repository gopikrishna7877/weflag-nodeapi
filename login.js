"use strict";
var express = require("express");
var bodyParser = require("body-parser");
var JWT_SECRET = 'asset_api_token';
var app = express();
app.use(bodyParser.json());
module.exports = app;

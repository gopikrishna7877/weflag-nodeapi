"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var express_1 = require("express");
var invitation_1 = require("../modals/invitation");
var APIError_1 = require("../utils/APIError");
var status_1 = require("../utils/status");
var errorMsg_1 = require("../utils/errorMsg");
var recruiters_1 = require("../modals/recruiters");
var email_1 = require("../utils/email");
var router = express_1.Router();
var body;
router.post("/getInvitaionList", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var iList, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, invitation_1.invitationSchema.find({}).sort({ "createdAt": -1 }).exec()];
            case 1:
                iList = _a.sent();
                res.send(new status_1.Status(200, iList));
                return [3 /*break*/, 3];
            case 2:
                error_1 = _a.sent();
                res.send(new APIError_1.APIError(201, error_1));
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
router.post("/sendInvitation", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var Email, isRecruiter, isInvitation, newInvitation, mailRes, error_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 11, , 12]);
                body = req.body;
                if (!!body.toemail) return [3 /*break*/, 1];
                res.send(new APIError_1.APIError(201, errorMsg_1.emailMsg));
                return [3 /*break*/, 10];
            case 1:
                if (!email_1.emailValidation(body.toemail)) return [3 /*break*/, 2];
                res.send(new APIError_1.APIError(201, errorMsg_1.emailErrMsg));
                return [3 /*break*/, 10];
            case 2:
                Email = body.toemail.toLowerCase();
                return [4 /*yield*/, recruiters_1.recuriterSchema.findOne({ email: Email }).exec()];
            case 3:
                isRecruiter = _a.sent();
                if (!!isRecruiter) return [3 /*break*/, 9];
                return [4 /*yield*/, invitation_1.invitationSchema.findOne({ toemail: Email }).exec()];
            case 4:
                isInvitation = _a.sent();
                if (!!isInvitation) return [3 /*break*/, 6];
                newInvitation = new invitation_1.invitationSchema({
                    toemail: Email,
                    cid: null,
                    invitedt: new Date().toISOString()
                });
                return [4 /*yield*/, newInvitation.save()];
            case 5:
                isInvitation = _a.sent();
                _a.label = 6;
            case 6:
                if (!isInvitation) return [3 /*break*/, 8];
                return [4 /*yield*/, email_1.sendInvitaionEmail(Email, isInvitation._id)];
            case 7:
                mailRes = _a.sent();
                if (mailRes.status == 200) {
                    res.send(new status_1.Status(200, isInvitation));
                }
                else {
                    res.send(new APIError_1.APIError(201, errorMsg_1.emailErrorMsg));
                }
                _a.label = 8;
            case 8: return [3 /*break*/, 10];
            case 9:
                res.send(new APIError_1.APIError(201, errorMsg_1.recuriterExistMsg));
                _a.label = 10;
            case 10: return [3 /*break*/, 12];
            case 11:
                error_2 = _a.sent();
                res.send(new APIError_1.APIError(201, error_2));
                return [3 /*break*/, 12];
            case 12: return [2 /*return*/];
        }
    });
}); });
module.exports = router;

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var express_1 = require("express");
var APIError_1 = require("../utils/APIError");
var status_1 = require("../utils/status");
var company_1 = require("../modals/company");
var router = express_1.Router();
router.post("/chartsData", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var obj, today, d, month, cyear, cmonth, barChartList, pieChartList_1, barChart, i, fy, cM, cY, r, obj_1, obj_2, pieChart, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 3, , 4]);
                obj = {};
                today = new Date();
                cyear = new Date().getFullYear();
                cmonth = new Date().getMonth() + 1;
                barChartList = [];
                pieChartList_1 = [];
                return [4 /*yield*/, company_1.companySchema.aggregate([{
                            $match: {
                                createdAt: {
                                    $gte: new Date(new Date().getFullYear(), new Date().getMonth() - 5),
                                    $lte: new Date(new Date().getFullYear(), new Date().getMonth() + 1)
                                }
                            }
                        },
                        {
                            $group: {
                                _id: {
                                    month: { $month: "$createdAt" },
                                    year: { $year: "$createdAt" }
                                },
                                count: { $sum: 1 }
                            }
                        }
                    ]).exec()];
            case 1:
                barChart = _a.sent();
                // if (barChart) {
                //     barChart.forEach(ele1 => {
                //         barChartList.push({
                //             month: ele1._id.month,
                //             year:ele1._id.year,
                //             count: ele1.count
                //         })
                //     })
                // }
                for (i = 6; i > 0; i--) {
                    fy = today.getFullYear();
                    cM = today.getMonth() + 1;
                    d = new Date(fy, cM - i, 1);
                    cY = d.getFullYear();
                    month = d.getMonth() + 1;
                    r = barChart.filter(function (x) { return x._id.month == month; });
                    if (r.length > 0) {
                        obj_1 = {
                            month: r[0]._id.month,
                            count: r[0].count,
                            year: r[0]._id.year
                        };
                        barChartList.push(obj_1);
                    }
                    else {
                        obj_2 = {
                            month: month,
                            count: 0,
                            year: cY
                        };
                        barChartList.push(obj_2);
                    }
                }
                return [4 /*yield*/, company_1.companySchema.aggregate([
                        {
                            $group: {
                                _id: {
                                    month: { $month: "$createdAt" },
                                    year: { $year: "$createdAt" }
                                },
                                count: { $sum: 1 }
                            }
                        }, {
                            $match: {
                                "_id.month": cmonth,
                                "_id.year": cyear
                            }
                        }
                    ]).exec()];
            case 2:
                pieChart = _a.sent();
                if (pieChart) {
                    pieChart.forEach(function (ele1) {
                        pieChartList_1.push({
                            month: ele1._id.month,
                            year: ele1._id.year,
                            count: ele1.count
                        });
                    });
                }
                obj["pieChart"] = pieChartList_1;
                obj["barChart"] = barChartList;
                res.send(new status_1.Status(200, obj));
                return [3 /*break*/, 4];
            case 3:
                error_1 = _a.sent();
                res.send(new APIError_1.APIError(201, error_1));
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); });
module.exports = router;

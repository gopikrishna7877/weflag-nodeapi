import { Router } from 'express';
import * as mongoose from 'mongoose';
import { APIError } from '../utils/APIError';
import { Status } from '../utils/status';
import * as errorMsg from '../utils/errorMsg';
import { companySchema } from '../modals/company';

let router = Router();

router.post("/chartsData", async (req, res, next) => {
    try {
        let obj = {};
        var today = new Date();
        var d;
        var month;
        let cyear = new Date().getFullYear();
        let cmonth = new Date().getMonth() + 1;
        let barChartList: any = [];
        let pieChartList: any = [];
        let barChart = await companySchema.aggregate([{
            $match: {
                createdAt: {
                    $gte: new Date(new Date().getFullYear(), new Date().getMonth() - 5),
                    $lte: new Date(new Date().getFullYear(), new Date().getMonth() + 1)
                }
            }
        },
        {
            $group: {
                _id: {
                    month: { $month: "$createdAt" },
                    year: { $year: "$createdAt" }
                },
                count: { $sum: 1 }
            }
        }
        ]).exec();
        // if (barChart) {
        //     barChart.forEach(ele1 => {
        //         barChartList.push({
        //             month: ele1._id.month,
        //             year:ele1._id.year,
        //             count: ele1.count
        //         })
        //     })
        // }

        for (var i = 6; i > 0; i--) {
            let fy = today.getFullYear();
            let cM = today.getMonth() + 1;
            d = new Date(fy, cM - i, 1);
            let cY = d.getFullYear();
            month = d.getMonth() + 1;
            let r = barChart.filter(x => x._id.month == month);
            if (r.length > 0) {
                let obj = {
                    month: r[0]._id.month,
                    count:  r[0].count,
                    year:  r[0]._id.year,
                }
                barChartList.push(obj);
            }
            else {
                let obj = {
                    month: month,
                    count: 0,
                    year: cY
                }
                barChartList.push(obj);
            }
        }

        let pieChart = await companySchema.aggregate([
            {
                $group: {
                    _id: {
                        month: { $month: "$createdAt" },
                        year: { $year: "$createdAt" }
                    },
                    count: { $sum: 1 }
                }
            }, {
                $match: {
                    "_id.month": cmonth,
                    "_id.year": cyear
                }
            }
        ]).exec();
        if (pieChart) {
            pieChart.forEach(ele1 => {
                pieChartList.push({
                    month: ele1._id.month,
                    year: ele1._id.year,
                    count: ele1.count
                })
            })
        }
        obj["pieChart"] = pieChartList;
        obj["barChart"] = barChartList;
        res.send(new Status(200, obj));
    }
    catch (error) {
        res.send(new APIError(201, error));
    }
})

export = router;
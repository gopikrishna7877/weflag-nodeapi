import { Router } from 'express';
import * as mongoose from 'mongoose';
import { APIError } from '../utils/APIError';
import { userNameMsg, passwordMsg, adminErrorMsg } from '../utils/errorMsg';
import { adminSchema } from '../modals/admin';
import { Status } from '../utils/status';
let router = Router();

let body;
router.post("/validate", async (req, res, next) => {
    try {
        body = req.body;
        if (!body.aname) {
            res.send(new APIError(201, userNameMsg));
        }
        else if (!body.apwd) {
            res.send(new APIError(201, passwordMsg));
        }
        else {
            let admin = await adminSchema.findOne({ aname: body.aname, apwd: body.apwd }).exec();
            if (admin) {
                res.send(new Status(200, admin));
            }
            else {
                res.send(new APIError(201, adminErrorMsg));
            }
        }
    } catch (error) {
        res.send(new APIError(201, error))

    }
})

export =router;
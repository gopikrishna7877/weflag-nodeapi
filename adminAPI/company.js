"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var express_1 = require("express");
var mongoose = require("mongoose");
var APIError_1 = require("../utils/APIError");
var status_1 = require("../utils/status");
var company_1 = require("../modals/company");
var recruiters_1 = require("../modals/recruiters");
var errorMsg_1 = require("../utils/errorMsg");
var email_1 = require("../utils/email");
var router = express_1.Router();
var body;
router.post("/addCompany", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var recruiterList, isExistRec, mailList, company, newCompany, companyDetails, obj, cnt, k, ele, isRecExists, newRecruiter, mailRes, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                recruiterList = [];
                isExistRec = "";
                mailList = "";
                _a.label = 1;
            case 1:
                _a.trys.push([1, 18, , 19]);
                body = req.body;
                if (!body.cname.trim()) {
                    res.send(new APIError_1.APIError(201, errorMsg_1.companyNameMsg));
                }
                else if (!body.city) {
                    res.send(new APIError_1.APIError(201, errorMsg_1.cityMsg));
                }
                if (!!body.rname.trim()) return [3 /*break*/, 2];
                res.send(new APIError_1.APIError(201, errorMsg_1.nameMsg));
                return [3 /*break*/, 17];
            case 2:
                if (!!body.email) return [3 /*break*/, 3];
                res.send(new APIError_1.APIError(201, errorMsg_1.emailMsg));
                return [3 /*break*/, 17];
            case 3:
                if (!email_1.emailValidation(body.email)) return [3 /*break*/, 4];
                res.send(new APIError_1.APIError(201, errorMsg_1.emailErrMsg));
                return [3 /*break*/, 17];
            case 4:
                if (!!body.con) return [3 /*break*/, 5];
                res.send(new APIError_1.APIError(201, errorMsg_1.contactNumberMsg));
                return [3 /*break*/, 17];
            case 5: return [4 /*yield*/, company_1.companySchema.findOne({ cname: body.cname }).exec()];
            case 6:
                company = _a.sent();
                if (!!company) return [3 /*break*/, 16];
                newCompany = new company_1.companySchema({
                    cname: body.cname,
                    caddr: body.caddr,
                    city: body.city,
                    crdt: new Date()
                });
                return [4 /*yield*/, newCompany.save()];
            case 7:
                companyDetails = _a.sent();
                req.body.cid = companyDetails._id;
                obj = {
                    rname: body.rname,
                    con: body.con,
                    email: body.email,
                    isprimary: 1
                };
                recruiterList = body.rList;
                recruiterList.unshift(obj);
                cnt = 0;
                k = 0;
                _a.label = 8;
            case 8:
                if (!(k < recruiterList.length)) return [3 /*break*/, 15];
                cnt++;
                ele = recruiterList[k];
                return [4 /*yield*/, recruiters_1.recuriterSchema.findOne({ email: ele.email.toLowerCase() }).exec()];
            case 9:
                isRecExists = _a.sent();
                if (!!isRecExists) return [3 /*break*/, 13];
                ele.cid = body.cid;
                ele.cname = body.cname;
                ele.caddr = body.caddr,
                    ele.city = body.city,
                    ele.rname = ele.rname;
                ele.email = ele.email.toLowerCase();
                ele.con = ele.con;
                mailList += ele.email + ",";
                newRecruiter = new recruiters_1.recuriterSchema(ele);
                return [4 /*yield*/, newRecruiter.save()];
            case 10:
                _a.sent();
                if (!(recruiterList.length == cnt)) return [3 /*break*/, 12];
                return [4 /*yield*/, email_1.recSuccessEmail(mailList)];
            case 11:
                mailRes = _a.sent();
                if (mailRes.status == 200)
                    res.send(new status_1.Status(200, companyDetails));
                else
                    res.send(new APIError_1.APIError(201, errorMsg_1.emailErrorMsg));
                _a.label = 12;
            case 12: return [3 /*break*/, 14];
            case 13:
                isExistRec += isRecExists.email + ",";
                if (recruiterList.length == cnt) {
                    res.send(new status_1.Status(200, { exists: isExistRec }));
                }
                _a.label = 14;
            case 14:
                k++;
                return [3 /*break*/, 8];
            case 15: return [3 /*break*/, 17];
            case 16:
                res.send(new APIError_1.APIError(201, errorMsg_1.companyExistsMsg));
                _a.label = 17;
            case 17: return [3 /*break*/, 19];
            case 18:
                error_1 = _a.sent();
                res.send(new APIError_1.APIError(201, error_1));
                return [3 /*break*/, 19];
            case 19: return [2 /*return*/];
        }
    });
}); });
router.post("/getCompanyRecruiters", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var body_1, rList, error_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 4, , 5]);
                body_1 = req.body;
                if (!!body_1.cid) return [3 /*break*/, 1];
                res.send(new APIError_1.APIError(201, errorMsg_1.companyIdMsg));
                return [3 /*break*/, 3];
            case 1: return [4 /*yield*/, recruiters_1.recuriterSchema.find({ cid: mongoose.Types.ObjectId(body_1.cid) }).exec()];
            case 2:
                rList = _a.sent();
                res.send(new status_1.Status(200, rList));
                _a.label = 3;
            case 3: return [3 /*break*/, 5];
            case 4:
                error_2 = _a.sent();
                res.send(new APIError_1.APIError(201, error_2));
                return [3 /*break*/, 5];
            case 5: return [2 /*return*/];
        }
    });
}); });
router.post("/getCompanyList", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var cList, error_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, company_1.companySchema.find({}).sort({ "createdAt": -1 }).exec()];
            case 1:
                cList = _a.sent();
                res.send(new status_1.Status(200, cList));
                return [3 /*break*/, 3];
            case 2:
                error_3 = _a.sent();
                res.send(new APIError_1.APIError(201, error_3));
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
router.post("/disabledOrEnabledCompany", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var body_2, status_2, text, emailLst, company, recEmails, r, mailRes, error_4;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 7, , 8]);
                body_2 = req.body;
                if (!!body_2.cid) return [3 /*break*/, 1];
                res.send(new APIError_1.APIError(201, errorMsg_1.companyIdMsg));
                return [3 /*break*/, 6];
            case 1:
                if (!!body_2.active) return [3 /*break*/, 2];
                res.send(new APIError_1.APIError(201, errorMsg_1.activeIdMsg));
                return [3 /*break*/, 6];
            case 2:
                text = "";
                emailLst = "";
                if (body_2.active == "0") {
                    status_2 = "Inactive";
                    text = ", further details contact admin";
                }
                else {
                    status_2 = "Activated";
                }
                return [4 /*yield*/, company_1.companySchema.findOneAndUpdate({
                        _id: mongoose.Types.ObjectId(body_2.cid)
                    }, {
                        $set: {
                            active: body_2.active
                        }
                    }).exec()];
            case 3:
                company = _a.sent();
                return [4 /*yield*/, recruiters_1.recuriterSchema.find({
                        cid: mongoose.Types.ObjectId(body_2.cid)
                    }, { email: 1, _id: 0 }).exec()];
            case 4:
                recEmails = _a.sent();
                if (recEmails.length > 0) {
                    for (r = 0; r < recEmails.length; r++) {
                        emailLst += recEmails[r].email + ",";
                    }
                }
                return [4 /*yield*/, email_1.activeOrInactiveEmail(emailLst, status_2, text)];
            case 5:
                mailRes = _a.sent();
                res.send(new status_1.Status(200, company));
                _a.label = 6;
            case 6: return [3 /*break*/, 8];
            case 7:
                error_4 = _a.sent();
                res.send(new APIError_1.APIError(201, error_4));
                return [3 /*break*/, 8];
            case 8: return [2 /*return*/];
        }
    });
}); });
router.post("/disabledOrEnabledRecruiter", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var body_3, status_3, text, recruiter, mailRes, error_5;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 6, , 7]);
                body_3 = req.body;
                if (!!body_3.rid) return [3 /*break*/, 1];
                res.send(new APIError_1.APIError(201, errorMsg_1.recuriterIdMsg));
                return [3 /*break*/, 5];
            case 1:
                if (!!body_3.active) return [3 /*break*/, 2];
                res.send(new APIError_1.APIError(201, errorMsg_1.activeIdMsg));
                return [3 /*break*/, 5];
            case 2:
                text = "";
                if (body_3.active == "0") {
                    status_3 = "Inactive";
                    text = ", further details contact admin";
                }
                else {
                    status_3 = "Activated";
                }
                return [4 /*yield*/, recruiters_1.recuriterSchema.findOneAndUpdate({
                        _id: mongoose.Types.ObjectId(body_3.rid)
                    }, {
                        $set: {
                            active: body_3.active
                        }
                    }).exec()];
            case 3:
                recruiter = _a.sent();
                return [4 /*yield*/, email_1.activeOrInactiveEmail(recruiter.email, status_3, text)];
            case 4:
                mailRes = _a.sent();
                res.send(new status_1.Status(200, recruiter));
                _a.label = 5;
            case 5: return [3 /*break*/, 7];
            case 6:
                error_5 = _a.sent();
                res.send(new APIError_1.APIError(201, error_5));
                return [3 /*break*/, 7];
            case 7: return [2 /*return*/];
        }
    });
}); });
router.post("/addRecruiter", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var company, recruiter, newRecruiter, r, mailRes, error_6;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 14, , 15]);
                body = req.body;
                if (!!body.cid) return [3 /*break*/, 1];
                res.send(new APIError_1.APIError(201, errorMsg_1.companyIdMsg));
                return [3 /*break*/, 13];
            case 1:
                if (!!body.rname) return [3 /*break*/, 2];
                res.send(new APIError_1.APIError(201, errorMsg_1.recuriterNameMsg));
                return [3 /*break*/, 13];
            case 2:
                if (!!body.email) return [3 /*break*/, 3];
                res.send(new APIError_1.APIError(201, errorMsg_1.emailMsg));
                return [3 /*break*/, 13];
            case 3:
                if (!email_1.emailValidation(body.email)) return [3 /*break*/, 4];
                res.send(new APIError_1.APIError(201, errorMsg_1.emailErrMsg));
                return [3 /*break*/, 13];
            case 4:
                if (!!body.con) return [3 /*break*/, 5];
                res.send(new APIError_1.APIError(201, errorMsg_1.contactNumberMsg));
                return [3 /*break*/, 13];
            case 5: return [4 /*yield*/, company_1.companySchema.findOne({ _id: mongoose.Types.ObjectId(body.cid) }).exec()];
            case 6:
                company = _a.sent();
                if (!company) return [3 /*break*/, 12];
                return [4 /*yield*/, recruiters_1.recuriterSchema.findOne({ email: body.email.toLowerCase() }).exec()];
            case 7:
                recruiter = _a.sent();
                if (!!recruiter) return [3 /*break*/, 10];
                body.cname = company.cname;
                body.caddr = company.caddr,
                    body.city = company.city,
                    body.email = body.email.toLowerCase();
                newRecruiter = new recruiters_1.recuriterSchema(body);
                return [4 /*yield*/, newRecruiter.save()];
            case 8:
                r = _a.sent();
                return [4 /*yield*/, email_1.recSuccessEmail(body.email)];
            case 9:
                mailRes = _a.sent();
                if (mailRes.status == 200)
                    res.send(new status_1.Status(200, r));
                else
                    res.send(new status_1.Status(200, r));
                return [3 /*break*/, 11];
            case 10:
                res.send(new APIError_1.APIError(201, errorMsg_1.recuriterExistMsg));
                _a.label = 11;
            case 11: return [3 /*break*/, 13];
            case 12:
                res.send(new APIError_1.APIError(201, errorMsg_1.companyNotExistsMsg));
                _a.label = 13;
            case 13: return [3 /*break*/, 15];
            case 14:
                error_6 = _a.sent();
                res.send(new APIError_1.APIError(201, error_6));
                return [3 /*break*/, 15];
            case 15: return [2 /*return*/];
        }
    });
}); });
router.post("/addCredits", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var company, newCredits, updateCompany, error_7;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 8, , 9]);
                body = req.body;
                if (!!body.cid) return [3 /*break*/, 1];
                res.send(new APIError_1.APIError(201, errorMsg_1.companyIdMsg));
                return [3 /*break*/, 7];
            case 1:
                if (!!body.credits) return [3 /*break*/, 2];
                res.send(new APIError_1.APIError(201, errorMsg_1.creditErrorMsg));
                return [3 /*break*/, 7];
            case 2: return [4 /*yield*/, company_1.companySchema.findOne({ _id: mongoose.Types.ObjectId(body.cid) }).exec()];
            case 3:
                company = _a.sent();
                if (!company) return [3 /*break*/, 6];
                newCredits = company.credits + Number(body.credits);
                return [4 /*yield*/, company_1.companySchema.updateOne({ _id: mongoose.Types.ObjectId(body.cid) }, {
                        $set: { credits: newCredits }
                    })];
            case 4:
                updateCompany = _a.sent();
                return [4 /*yield*/, company_1.companySchema.findOne({ _id: mongoose.Types.ObjectId(body.cid) }).exec()];
            case 5:
                company = _a.sent();
                res.send(new status_1.Status(200, company));
                return [3 /*break*/, 7];
            case 6:
                res.send(new APIError_1.APIError(201, errorMsg_1.companyNotExistsMsg));
                _a.label = 7;
            case 7: return [3 /*break*/, 9];
            case 8:
                error_7 = _a.sent();
                res.send(new APIError_1.APIError(201, error_7));
                return [3 /*break*/, 9];
            case 9: return [2 /*return*/];
        }
    });
}); });
module.exports = router;

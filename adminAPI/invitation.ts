import { Router } from 'express';
import { invitationSchema } from '../modals/invitation';
import { APIError } from '../utils/APIError';
import { Status } from '../utils/status';
import { emailMsg, recuriterExistMsg, emailErrorMsg, emailErrMsg } from '../utils/errorMsg';
import { recuriterSchema } from '../modals/recruiters';
import { sendInvitaionEmail, emailValidation } from '../utils/email';
let router = Router();
let body;
router.post("/getInvitaionList", async (req, res, next) => {
    try {
        let iList = await invitationSchema.find({}).sort({ "createdAt": -1 }).exec();
        res.send(new Status(200, iList));
    } catch (error) {
        res.send(new APIError(201, error));
    }
});
router.post("/sendInvitation", async (req, res, next) => {
    try {
        body = req.body;
        if (!body.toemail) {
            res.send(new APIError(201, emailMsg));
        }
        else if (emailValidation(body.toemail)) {
            res.send(new APIError(201, emailErrMsg))
        }
        else {
            let Email = body.toemail.toLowerCase();
            let isRecruiter = await recuriterSchema.findOne({ email: Email }).exec();
            if (!isRecruiter) {
                let isInvitation = await invitationSchema.findOne({ toemail: Email }).exec();
                if (!isInvitation) {
                    let newInvitation = new invitationSchema({
                        toemail: Email,
                        cid: null,
                        invitedt: new Date().toISOString()
                    })
                    isInvitation = await newInvitation.save();
                }
                if (isInvitation) {
                    let mailRes = await sendInvitaionEmail(Email, isInvitation._id);
                    if (mailRes.status == 200) {
                        res.send(new Status(200, isInvitation));
                    }
                    else {
                        res.send(new APIError(201, emailErrorMsg))
                    }
                }
            }
            else {
                res.send(new APIError(201, recuriterExistMsg));
            }
        }
    } catch (error) {
        res.send(new APIError(201, error));
    }
});
export =router;
import * as express from 'express';
import * as bodyParser from "body-parser";
import * as companyRoutes from '../adminAPI/company';
import * as invitationRoutes from '../adminAPI/invitation';
import * as dashboardRoutes from '../adminAPI/dashboard';

let app = express();
app.use('/company', companyRoutes);
app.use('/invitation', invitationRoutes);
app.use('/dashboard', dashboardRoutes);
export = app;
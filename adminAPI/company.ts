import { Router } from 'express';
import * as mongoose from 'mongoose';
import { APIError } from '../utils/APIError';
import { Status } from '../utils/status';
import { companySchema } from '../modals/company';
import { recuriterSchema } from '../modals/recruiters';
import { companyNameMsg, cityMsg, nameMsg, emailMsg, emailErrMsg, contactNumberMsg, companyExistsMsg, recuriterIdMsg, activeIdMsg, companyIdMsg, recuriterNameMsg, companyNotExistsMsg, recuriterNotExistMsg, recuriterExistMsg, creditErrorMsg, emailErrorMsg } from '../utils/errorMsg';
import { emailValidation, recSuccessEmail, activeOrInactiveEmail } from '../utils/email';
let router = Router();

let body;
router.post("/addCompany", async (req, res, next) => {
    let recruiterList = [];
    let isExistRec = "";
    let mailList = "";
    try {
        body = req.body;
        if (!body.cname.trim()) {
            res.send(new APIError(201, companyNameMsg))
        }
        else if (!body.city) {
            res.send(new APIError(201, cityMsg))
        }
        if (!body.rname.trim()) {
            res.send(new APIError(201, nameMsg))
        }
        else if (!body.email) {
            res.send(new APIError(201, emailMsg))
        }
        else if (emailValidation(body.email)) {
            res.send(new APIError(201, emailErrMsg))
        }
        else if (!body.con) {
            res.send(new APIError(201, contactNumberMsg))
        }
        else {
            let company = await companySchema.findOne({ cname: body.cname }).exec();
            if (!company) {
                var newCompany = new companySchema({
                    cname: body.cname,
                    caddr: body.caddr,
                    city: body.city,
                    crdt: new Date()
                });
                let companyDetails = await newCompany.save();
                req.body.cid = companyDetails._id;
                let obj = {
                    rname: body.rname,
                    con: body.con,
                    email: body.email,
                    isprimary: 1
                }
                recruiterList = body.rList;
                recruiterList.unshift(obj);
                let cnt = 0;

                for (let k = 0; k < recruiterList.length; k++) {
                    cnt++;
                    let ele = recruiterList[k];
                    let isRecExists: any = await recuriterSchema.findOne({ email: ele.email.toLowerCase() }).exec();
                    if (!isRecExists) {
                        ele.cid = body.cid;
                        ele.cname = body.cname;
                        ele.caddr = body.caddr,
                            ele.city = body.city,
                            ele.rname = ele.rname;
                        ele.email = ele.email.toLowerCase();
                        ele.con = ele.con;
                        mailList += ele.email + ",";
                        let newRecruiter = new recuriterSchema(ele)
                        await newRecruiter.save();
                        if (recruiterList.length == cnt) {
                            let mailRes = await recSuccessEmail(mailList);
                            if (mailRes.status == 200)
                                res.send(new Status(200, companyDetails))
                            else
                                res.send(new APIError(201, emailErrorMsg))
                        }
                    }
                    else {
                        isExistRec += isRecExists.email + ",";
                        if (recruiterList.length == cnt) {
                            res.send(new Status(200, { exists: isExistRec }))
                        }
                    }
                }
            }
            else {
                res.send(new APIError(201, companyExistsMsg))
            }
        }
    } catch (error) {
        res.send(new APIError(201, error))

    }
});

router.post("/getCompanyRecruiters", async (req, res, next) => {
    try {
        let body = req.body;
        if (!body.cid) {
            res.send(new APIError(201, companyIdMsg));
        }
        else {
            let rList = await recuriterSchema.find({ cid: mongoose.Types.ObjectId(body.cid) }).exec();
            res.send(new Status(200, rList));
        }

    } catch (error) {
        res.send(new APIError(201, error));
    }
})

router.post("/getCompanyList", async (req, res, next) => {
    try {
        let cList = await companySchema.find({}).sort({ "createdAt": -1 }).exec();
        res.send(new Status(200, cList));
    } catch (error) {
        res.send(new APIError(201, error));
    }
});

router.post("/disabledOrEnabledCompany", async (req, res, next) => {
    try {
        let body = req.body;
        if (!body.cid) {
            res.send(new APIError(201, companyIdMsg));
        }
        else if (!body.active) {
            res.send(new APIError(201, activeIdMsg));
        }
        else {
            let status;
            let text = "";
            let emailLst = "";
            if (body.active == "0") {
                status = "Inactive";
                text = ", further details contact admin";
            }
            else {
                status = "Activated";
            }
            let company = await companySchema.findOneAndUpdate(
                {
                    _id: mongoose.Types.ObjectId(body.cid)
                },
                {
                    $set: {
                        active: body.active
                    }
                }
            ).exec();
            let recEmails: any = await recuriterSchema.find({
                cid: mongoose.Types.ObjectId(body.cid)
            }, { email: 1, _id: 0 }).exec();
            if (recEmails.length > 0) {
                for (let r = 0; r < recEmails.length; r++) {
                    emailLst += recEmails[r].email + ",";
                }
            }
            let mailRes = await activeOrInactiveEmail(emailLst, status, text);
            res.send(new Status(200, company));

        }

    } catch (error) {
        res.send(new APIError(201, error));
    }
});

router.post("/disabledOrEnabledRecruiter", async (req, res, next) => {
    try {
        let body = req.body;
        if (!body.rid) {
            res.send(new APIError(201, recuriterIdMsg));
        }
        else if (!body.active) {
            res.send(new APIError(201, activeIdMsg));
        }
        else {
            let status;
            let text = "";
            if (body.active == "0") {
                status = "Inactive";
                text = ", further details contact admin";
            }
            else {
                status = "Activated";
            }
            let recruiter: any = await recuriterSchema.findOneAndUpdate(
                {
                    _id: mongoose.Types.ObjectId(body.rid)
                },
                {
                    $set: {
                        active: body.active
                    }
                }
            ).exec();
            let mailRes = await activeOrInactiveEmail(recruiter.email, status, text);
            res.send(new Status(200, recruiter));
        }

    } catch (error) {
        res.send(new APIError(201, error));
    }
});

router.post("/addRecruiter", async (req, res, next) => {
    try {
        body = req.body;
        if (!body.cid) {
            res.send(new APIError(201, companyIdMsg));
        }
        else if (!body.rname) {
            res.send(new APIError(201, recuriterNameMsg));
        }
        else if (!body.email) {
            res.send(new APIError(201, emailMsg));
        }
        else if (emailValidation(body.email)) {
            res.send(new APIError(201, emailErrMsg))
        }
        else if (!body.con) {
            res.send(new APIError(201, contactNumberMsg));
        }
        else {
            let company: any = await companySchema.findOne({ _id: mongoose.Types.ObjectId(body.cid) }).exec();
            if (company) {
                let recruiter = await recuriterSchema.findOne({ email: body.email.toLowerCase() }).exec();
                if (!recruiter) {
                    body.cname = company.cname;
                    body.caddr = company.caddr,
                        body.city = company.city,
                        body.email = body.email.toLowerCase();
                    let newRecruiter = new recuriterSchema(body)
                    let r = await newRecruiter.save();
                    let mailRes = await recSuccessEmail(body.email);
                    if (mailRes.status == 200)
                        res.send(new Status(200, r))
                    else res.send(new Status(200, r));
                }
                else {
                    res.send(new APIError(201, recuriterExistMsg));
                }

            }
            else {
                res.send(new APIError(201, companyNotExistsMsg));
            }
        }

    } catch (error) {
        res.send(new APIError(201, error));

    }
})

router.post("/addCredits", async (req, res, next) => {
    try {
        body = req.body;
        if (!body.cid) {
            res.send(new APIError(201, companyIdMsg));
        }
        else if (!body.credits) {
            res.send(new APIError(201, creditErrorMsg));
        }
        else {
            let company: any = await companySchema.findOne({ _id: mongoose.Types.ObjectId(body.cid) }).exec();
            if (company) {
                let newCredits = company.credits + Number(body.credits);
                let updateCompany = await companySchema.updateOne(
                    { _id: mongoose.Types.ObjectId(body.cid) },
                    {
                        $set: { credits: newCredits }
                    });
                company = await companySchema.findOne({ _id: mongoose.Types.ObjectId(body.cid) }).exec()
                res.send(new Status(200, company))
            }
            else {
                res.send(new APIError(201, companyNotExistsMsg));
            }

        }
    } catch (error) {
        res.send(new APIError(201, error));
    }
})


export =router;
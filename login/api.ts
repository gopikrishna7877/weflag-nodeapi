import { Router } from 'express';
let router = Router();
import * as mongoose from 'mongoose';
import { sendEmail } from '../utils/email';
import { generateToken } from '../utils/jwt';
import { recuriterSchema } from '../modals/recruiters';
import { APIError } from '../utils/APIError';
import { Status } from '../utils/status';
import { emailMsg, invalidOtpMsg, OtpExpireMsg,companyNotExistsAdminMsg, userNotExistsMsg, OtpSendToMailMsg, adminActiveMsg, companyNotExistsMsg } from '../utils/errorMsg';
import { companySchema } from '../modals/company';


router.post('/validate', async (req, res, next) => {
    try {
        let body = req.body;
        if (!body.email) {
            res.send(new APIError(201, emailMsg))
        }
        else {
            req.body.email = body.email.toLowerCase();
            let user: any = await recuriterSchema.findOne(req.body).exec();
            if (user) {
                let company: any = await companySchema.findOne({ _id: user.cid }).exec();
                if(company){
                    if (user.active == 1 && company.active == 1) {
                        var mailRes = await sendEmail(req.body.email, user.rname);
                        if (mailRes.status === 200) {
                            user.otp = {
                                otp: mailRes.OTP,
                                tms: Date.now()
                            };
                            await recuriterSchema.updateOne({ email: req.body.email }, { $set: { otp: user.otp } });
                            // let token = await generateToken(user);
                            res.send(new Status(200, { rid: user._id }))
                            // res.send(new Status(200, { rid: user._id, access_token: token }))
                        }
                        else {
                            res.send(new APIError(201, OtpSendToMailMsg))
                        }
                    }
                    else {
                        res.send(new APIError(201, adminActiveMsg))
                    }
                }
                else{
                    res.send(new APIError(201, companyNotExistsAdminMsg)); 
                }
               
            }
            else {
                res.send(new APIError(201, userNotExistsMsg));
            }
        }
    } catch (error) {
        // res.send(new APIError(201, error));
        next(error)
    }

});


export = router;
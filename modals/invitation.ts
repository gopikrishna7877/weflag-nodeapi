import * as mongoose from 'mongoose';
import plugin from 'mongoose-transform';
import * as uniqueValidator from 'mongoose-unique-validator';

const iSchema = new mongoose.Schema({
    cid: { type: mongoose.Schema.Types.ObjectId, ref: 'company' },
    toemail: { type: String },
    invitedt: { type: Date },
    registerdt: { type: Date },
    isregister: { type: Number, default: 0 },
    sentby: { type: String, default: 'admin' }
});
iSchema.plugin(plugin);
iSchema.plugin(uniqueValidator, { message: `{PATH} already exists` });

export var invitationSchema = mongoose.model("invitationdetails", iSchema, "invitationdetails");
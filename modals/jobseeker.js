"use strict";
exports.__esModule = true;
var mongoose = require("mongoose");
var mongoose_transform_1 = require("mongoose-transform");
var uniqueValidator = require("mongoose-unique-validator");
var jobSrSchema = new mongoose.Schema({
    rid: { type: mongoose.Schema.Types.ObjectId, ref: 'recruiter' },
    cid: { type: mongoose.Schema.Types.ObjectId, ref: 'company' },
    jsname: { type: String },
    jsemail: { type: String },
    jscon: { type: String },
    jsalcon: { type: String, "default": null },
    jsaorp: { type: String, "default": null },
    cdate: { type: String },
    udate: { type: String },
    fsts: { type: Number },
    fstsCnt: { type: Number },
    active: { type: Number, "default": 1 }
});
jobSrSchema.plugin(mongoose_transform_1["default"]);
jobSrSchema.plugin(uniqueValidator, { message: "{PATH} already exists" });
exports.jobSeekerSchema = mongoose.model("jobseeker", jobSrSchema, "jobseeker");

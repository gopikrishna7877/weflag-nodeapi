"use strict";
exports.__esModule = true;
var mongoose = require("mongoose");
var mongoose_transform_1 = require("mongoose-transform");
var uniqueValidator = require("mongoose-unique-validator");
var cSchema = new mongoose.Schema({
    cname: { type: String },
    domain: { type: String },
    caddr: { type: String },
    city: { type: String },
    credits: { type: Number, "default": 100 },
    crdt: { type: String, "default": new Date() },
    crby: { type: String, "default": "admin" },
    updt: { type: String },
    upby: { type: String },
    active: { type: Number, "default": 1 }
});
cSchema.plugin(mongoose_transform_1["default"]);
cSchema.plugin(uniqueValidator, { message: "{PATH} already exists" });
exports.companySchema = mongoose.model("company", cSchema, 'company');

import * as mongoose from 'mongoose';
import plugin from 'mongoose-transform';
import * as uniqueValidator from "mongoose-unique-validator"

const trnCreditsSchema = new mongoose.Schema({
    cid: { type: mongoose.Schema.Types.ObjectId, ref: 'company' },
    rid: { type: mongoose.Schema.Types.ObjectId, ref: 'recruiter' },
    jid: { type: mongoose.Schema.Types.ObjectId, ref: 'jobseeker' },
    rname: { type: String },
    jsname: { type: String },
    rcon: { type: String },
    credits: { type: Number },
    trtype: { type: String },
    trdt: { type: Date, default: new Date() }
});transtionCreditsSchema

trnCreditsSchema.plugin(plugin);
trnCreditsSchema.plugin(uniqueValidator, { message: `{PATH} already exists` });

export var transtionCreditsSchema = mongoose.model("crtrndetails", trnCreditsSchema, "crtrndetails");
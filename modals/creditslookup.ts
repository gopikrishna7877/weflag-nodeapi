import * as mongoose from 'mongoose';
import plugin from 'mongoose-transform';
import * as uniqueValidator from "mongoose-unique-validator";
const crLkSchema = new mongoose.Schema({
    noshow: { type: Number },
    notjoined: { type: Number },
    joined: { type: Number },
    search: { type: Number },
    updt: { type: Date },
    upby: { type: String, default: "admin" }
});
crLkSchema.plugin(plugin);
crLkSchema.plugin(uniqueValidator, { message: `{PATH} already exists` });
export var crLookSchema = mongoose.model("creditslookup", crLkSchema, "creditslookup")
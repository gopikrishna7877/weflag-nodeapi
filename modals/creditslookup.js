"use strict";
exports.__esModule = true;
var mongoose = require("mongoose");
var mongoose_transform_1 = require("mongoose-transform");
var uniqueValidator = require("mongoose-unique-validator");
var crLkSchema = new mongoose.Schema({
    noshow: { type: Number },
    notjoined: { type: Number },
    joined: { type: Number },
    search: { type: Number },
    updt: { type: Date },
    upby: { type: String, "default": "admin" }
});
crLkSchema.plugin(mongoose_transform_1["default"]);
crLkSchema.plugin(uniqueValidator, { message: "{PATH} already exists" });
exports.crLookSchema = mongoose.model("creditslookup", crLkSchema, "creditslookup");

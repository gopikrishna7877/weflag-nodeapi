"use strict";
exports.__esModule = true;
var mongoose = require("mongoose");
var mongoose_transform_1 = require("mongoose-transform");
var uniqueValidator = require("mongoose-unique-validator");
var fSchema = new mongoose.Schema({
    cid: { type: mongoose.Schema.Types.ObjectId, ref: 'company' },
    rid: { type: mongoose.Schema.Types.ObjectId, ref: 'recruiter' },
    jid: { type: mongoose.Schema.Types.ObjectId, ref: 'jobseeker' },
    fid: { type: mongoose.Schema.Types.ObjectId, ref: 'flagdetails' },
    path: { type: String }
});
fSchema.plugin(mongoose_transform_1["default"]);
fSchema.plugin(uniqueValidator, { message: "{PATH} already exists" });
exports.attachmentSchema = mongoose.model("attachmentdetails", fSchema, "attachmentdetails");

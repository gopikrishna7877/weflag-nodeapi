"use strict";
exports.__esModule = true;
var mongoose = require("mongoose");
var mongoose_transform_1 = require("mongoose-transform");
var uniqueValidator = require("mongoose-unique-validator");
var luSchema = new mongoose.Schema({
    lkpid: { type: Number },
    lkpval: { type: String },
    lkpkey: { type: String },
    active: { type: Number, "default": 1 }
});
luSchema.plugin(mongoose_transform_1["default"]);
luSchema.plugin(uniqueValidator, { message: "{PATH} already exists" });
exports.lookUpSchema = mongoose.model("lookup", luSchema, "lookup");

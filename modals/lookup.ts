import * as mongoose from 'mongoose';
import plugin from 'mongoose-transform';
import * as uniqueValidator from 'mongoose-unique-validator';

const luSchema = new mongoose.Schema({
    lkpid: { type: Number },
    lkpval: { type: String },
    lkpkey: { type: String },
    active: { type: Number, default: 1 }
})

luSchema.plugin(plugin);
luSchema.plugin(uniqueValidator, { message: `{PATH} already exists` });

export var lookUpSchema = mongoose.model("lookup", luSchema, "lookup")

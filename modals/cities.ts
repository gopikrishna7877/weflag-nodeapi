import * as mongoose from 'mongoose';
import plugin from 'mongoose-transform';
import * as uniqueValidator from "mongoose-unique-validator";
const cSchema = new mongoose.Schema({
    _id: { type: mongoose.Schema.Types.ObjectId, ref: 'city' },
    ctyid: { type: Number },
    name: { type: String }

});
cSchema.plugin(plugin);
cSchema.plugin(uniqueValidator, { message: `{PATH} already exists` });

export var citiesSchema = mongoose.model("cities", cSchema, "city")
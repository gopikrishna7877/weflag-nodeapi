"use strict";
exports.__esModule = true;
var mongoose = require("mongoose");
var mongoose_transform_1 = require("mongoose-transform");
var uniqueValidator = require("mongoose-unique-validator");
var Otp = new mongoose.Schema({
    otp: { type: String },
    tms: { type: String }
});
var schema = new mongoose.Schema({
    cname: { Type: String },
    con: { Type: String },
    caddr: { Type: String },
    email: { Type: String },
    utype: { Type: String },
    cr: { Type: String },
    otp: Otp,
    cr_sold: [],
    cr_pur: [],
    cdate: String,
    active: Number
});
schema.plugin(mongoose_transform_1["default"]);
schema.plugin(uniqueValidator, { message: "{PATH} already exists" });
exports.employersSchema = mongoose.model("userregistration", schema);

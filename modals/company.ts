import * as mongoose from 'mongoose';
import plugin from 'mongoose-transform';
import * as uniqueValidator from "mongoose-unique-validator";

const cSchema = new mongoose.Schema({
    cname: { type: String },
    domain: { type: String },
    caddr: { type: String },
    city: { type: String },
    credits: { type: Number, default: 100 },
    crdt: { type: String, default: new Date() },
    crby: { type: String, default: "admin" },
    updt: { type: String },
    upby: { type: String },
    active: { type: Number, default: 1 }
})

cSchema.plugin(plugin);
cSchema.plugin(uniqueValidator, { message: `{PATH} already exists` });

export var companySchema = mongoose.model("company", cSchema, 'company')



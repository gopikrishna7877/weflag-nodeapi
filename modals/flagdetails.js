"use strict";
exports.__esModule = true;
var mongoose = require("mongoose");
var mongoose_transform_1 = require("mongoose-transform");
var uniqueValidator = require("mongoose-unique-validator");
var flagSchema = new mongoose.Schema({
    cid: { type: mongoose.Schema.Types.ObjectId, ref: 'company' },
    rid: { type: mongoose.Schema.Types.ObjectId, ref: 'recruiter' },
    jid: { type: mongoose.Schema.Types.ObjectId, ref: 'jobseeker' },
    cdate: { type: String },
    udate: { type: String },
    fsts: { type: Number },
    isd: { type: String },
    cname: { type: String },
    whd: { type: String },
    ord: { type: String, "default": null },
    oad: { type: String, "default": null },
    pjd: { type: String, "default": null },
    infdt: { type: String, "default": null },
    idt: { type: String, "default": null },
    ajdt: { type: String, "default": null },
    active: { type: Number, "default": 1 }
});
flagSchema.plugin(mongoose_transform_1["default"]);
flagSchema.plugin(uniqueValidator, { message: "{PATH} already exists" });
exports.flagDetailsSchema = mongoose.model("flagdetails", flagSchema, "flagdetails");

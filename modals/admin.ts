import * as mongoose from 'mongoose';
import plugin from 'mongoose-transform';
import * as uniqueValidator from "mongoose-unique-validator";

const adminDetailsSchema = new mongoose.Schema({
    aname: { type: String },
    apwd: { type: String },
    aemail: { type: String },
    acrdt: { type: String },
    acrby: { type: String },
    aupdt: { type: String },
    aupby: { type: String },
    active: { type: String },
});

adminDetailsSchema.plugin(plugin);
adminDetailsSchema.plugin(uniqueValidator, { message: `{PATH} already exists` });

export var adminSchema = mongoose.model("admindetails", adminDetailsSchema, 'admindetails');
"use strict";
exports.__esModule = true;
var mongoose = require("mongoose");
var mongoose_transform_1 = require("mongoose-transform");
var uniqueValidator = require("mongoose-unique-validator");
var trnCreditsSchema = new mongoose.Schema({
    cid: { type: mongoose.Schema.Types.ObjectId, ref: 'company' },
    rid: { type: mongoose.Schema.Types.ObjectId, ref: 'recruiter' },
    jid: { type: mongoose.Schema.Types.ObjectId, ref: 'jobseeker' },
    rname: { type: String },
    jsname: { type: String },
    rcon: { type: String },
    credits: { type: Number },
    trtype: { type: String },
    trdt: { type: Date, "default": new Date() }
});
exports.transtionCreditsSchema;
trnCreditsSchema.plugin(mongoose_transform_1["default"]);
trnCreditsSchema.plugin(uniqueValidator, { message: "{PATH} already exists" });
exports.transtionCreditsSchema = mongoose.model("crtrndetails", trnCreditsSchema, "crtrndetails");

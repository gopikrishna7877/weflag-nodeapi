"use strict";
exports.__esModule = true;
var mongoose = require("mongoose");
var mongoose_transform_1 = require("mongoose-transform");
var uniqueValidator = require("mongoose-unique-validator");
var iSchema = new mongoose.Schema({
    cid: { type: mongoose.Schema.Types.ObjectId, ref: 'company' },
    toemail: { type: String },
    invitedt: { type: Date },
    registerdt: { type: Date },
    isregister: { type: Number, "default": 0 },
    sentby: { type: String, "default": 'admin' }
});
iSchema.plugin(mongoose_transform_1["default"]);
iSchema.plugin(uniqueValidator, { message: "{PATH} already exists" });
exports.invitationSchema = mongoose.model("invitationdetails", iSchema, "invitationdetails");

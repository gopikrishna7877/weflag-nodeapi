"use strict";
exports.__esModule = true;
var mongoose = require("mongoose");
var mongoose_transform_1 = require("mongoose-transform");
var uniqueValidator = require("mongoose-unique-validator");
var cSchema = new mongoose.Schema({
    _id: { type: mongoose.Schema.Types.ObjectId, ref: 'city' },
    ctyid: { type: Number },
    name: { type: String }
});
cSchema.plugin(mongoose_transform_1["default"]);
cSchema.plugin(uniqueValidator, { message: "{PATH} already exists" });
exports.citiesSchema = mongoose.model("cities", cSchema, "city");

"use strict";
exports.__esModule = true;
var mongoose = require("mongoose");
var mongoose_transform_1 = require("mongoose-transform");
var uniqueValidator = require("mongoose-unique-validator");
var OtpDetails = {
    otp: { type: String },
    tms: { type: String }
};
var rSchema = new mongoose.Schema({
    cid: { type: mongoose.Schema.Types.ObjectId, ref: 'company' },
    con: { type: String },
    cname: { type: String },
    rname: { type: String },
    caddr: { type: String },
    city: { type: String },
    email: { type: String },
    utype: { type: Number, "default": 2 },
    credits: { type: Number, "default": 100 },
    otp: OtpDetails,
    crdt: { type: String, "default": new Date() },
    crby: { type: String, "default": "admin" },
    updt: { type: String },
    upby: { type: String },
    isprimary: { type: Number, "default": 0 },
    active: { type: Number, "default": 1 }
});
rSchema.plugin(mongoose_transform_1["default"]);
rSchema.plugin(uniqueValidator, { message: "{PATH} already exists" });
exports.recuriterSchema = mongoose.model("recruiter", rSchema, 'recruiter');

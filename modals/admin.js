"use strict";
exports.__esModule = true;
var mongoose = require("mongoose");
var mongoose_transform_1 = require("mongoose-transform");
var uniqueValidator = require("mongoose-unique-validator");
var adminDetailsSchema = new mongoose.Schema({
    aname: { type: String },
    apwd: { type: String },
    aemail: { type: String },
    acrdt: { type: String },
    acrby: { type: String },
    aupdt: { type: String },
    aupby: { type: String },
    active: { type: String }
});
adminDetailsSchema.plugin(mongoose_transform_1["default"]);
adminDetailsSchema.plugin(uniqueValidator, { message: "{PATH} already exists" });
exports.adminSchema = mongoose.model("admindetails", adminDetailsSchema, 'admindetails');

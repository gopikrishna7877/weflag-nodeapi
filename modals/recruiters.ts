import * as mongoose from 'mongoose';
import plugin from 'mongoose-transform';
import * as uniqueValidator from "mongoose-unique-validator"


const OtpDetails = {
    otp: { type: String },
    tms: { type: String }
}

const rSchema = new mongoose.Schema({
    cid: { type: mongoose.Schema.Types.ObjectId, ref: 'company' },
    con: { type: String },
    cname: { type: String },
    rname: { type: String },
    caddr: { type: String },
    city: { type: String },
    email: { type: String },
    utype: { type: Number, default: 2 },
    credits: { type: Number, default: 100 },
    otp: OtpDetails,
    crdt: { type: String, default: new Date() },
    crby: { type: String, default: "admin" },
    updt: { type: String },
    upby: { type: String },
    isprimary: { type: Number, default: 0 },
    active: { type: Number, default: 1 },
})

rSchema.plugin(plugin);
rSchema.plugin(uniqueValidator, { message: `{PATH} already exists` });

export var recuriterSchema = mongoose.model("recruiter", rSchema, 'recruiter')
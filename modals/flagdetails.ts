import * as mongoose from 'mongoose';
import plugin from 'mongoose-transform';
import * as uniqueValidator from 'mongoose-unique-validator';

const flagSchema = new mongoose.Schema({
    cid: { type: mongoose.Schema.Types.ObjectId, ref: 'company' },
    rid: { type: mongoose.Schema.Types.ObjectId, ref: 'recruiter' },    
    jid: { type: mongoose.Schema.Types.ObjectId, ref: 'jobseeker' },
    cdate: { type: String },
    udate: { type: String },
    fsts: { type: Number },
    isd: { type: String }, //Interview Scheduler date
    cname: { type: String },//Company Name
    whd: { type: String },//What Happened
    ord: { type: String, default: null },//offer received date
    oad: { type: String, default: null },//offer accepted date
    pjd: { type: String, default: null },//proposed joining date
    infdt: { type: String, default: null },//informed date
    idt: { type: String, default: null },//interviewed date   
    ajdt: { type: String, default: null },//actual joining date        
    active: { type: Number, default: 1 }
});

flagSchema.plugin(plugin);
flagSchema.plugin(uniqueValidator, { message: `{PATH} already exists` });

export var flagDetailsSchema = mongoose.model("flagdetails", flagSchema, "flagdetails")

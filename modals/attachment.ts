import * as mongoose from 'mongoose';
import plugin from 'mongoose-transform';
import * as uniqueValidator from 'mongoose-unique-validator';

const fSchema = new mongoose.Schema({
    cid: { type: mongoose.Schema.Types.ObjectId, ref: 'company' },
    rid: { type: mongoose.Schema.Types.ObjectId, ref: 'recruiter' },
    jid: { type: mongoose.Schema.Types.ObjectId, ref: 'jobseeker' },
    fid: { type: mongoose.Schema.Types.ObjectId, ref: 'flagdetails' },
    path: { type: String }
});
fSchema.plugin(plugin);
fSchema.plugin(uniqueValidator, { message: `{PATH} already exists` });
export var attachmentSchema = mongoose.model("attachmentdetails", fSchema, "attachmentdetails")

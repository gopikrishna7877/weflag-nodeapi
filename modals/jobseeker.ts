import * as mongoose from 'mongoose';
import plugin from 'mongoose-transform';
import * as uniqueValidator from 'mongoose-unique-validator';

const jobSrSchema = new mongoose.Schema({
    rid: { type: mongoose.Schema.Types.ObjectId, ref: 'recruiter' },
    cid: { type: mongoose.Schema.Types.ObjectId, ref: 'company' },
    jsname: { type: String },
    jsemail: { type: String },
    jscon: { type: String },
    jsalcon: { type: String,default:null },
    jsaorp: { type: String ,default:null },
    cdate: { type: String },
    udate: { type: String },
    fsts: { type: Number },
    fstsCnt:{type:Number},
    active: { type: Number, default: 1 }
});

jobSrSchema.plugin(plugin);
jobSrSchema.plugin(uniqueValidator, { message: `{PATH} already exists` });

export var jobSeekerSchema = mongoose.model("jobseeker", jobSrSchema, "jobseeker")

import { Router } from 'express';
let router = Router();
import { Status } from '../utils/status';
import { APIError } from '../utils/APIError';
import { sendEmail, emailValidation, recSuccessEmail } from '../utils/email';
import { recuriterSchema } from '../modals/recruiters';
import { companySchema } from '../modals/company';
import * as mongoose from 'mongoose';
import { nameMsg, emailMsg, contactNumberMsg, companyNameMsg, companyExistsMsg, cityMsg, OtpSendToMailMsg, userExistsMsg, recuriterIdMsg, OtpMsg, invalidOtpMsg, userNotExistsMsg, OtpExpireMsg, OtpResendMsg, OtpExpTime, freeEmailList, emailErrMsg, recuriterNotExistMsg, idMsg, invalidDetailsMsg, recuriterExistMsg, emailErrorMsg } from '../utils/errorMsg';
import { invitationSchema } from '../modals/invitation';

router.post("/_addRecruiter", async (req, res) => {
    try {
        let body = req.body;
        if (!body.rname.trim()) {
            res.send(new APIError(201, nameMsg))
        }
        else if (!body.email) {
            res.send(new APIError(201, emailMsg))
        }
        else if (emailValidation(body.email)) {
            res.send(new APIError(201, emailErrMsg))
        }
        else if (!body.con) {
            res.send(new APIError(201, contactNumberMsg))
        }
        else if (!body.cname.trim()) {
            res.send(new APIError(201, companyNameMsg))
        }
        else if (!body.city) {
            res.send(new APIError(201, cityMsg))
        }
        else {
            let isRecuriterExist = await recuriterSchema.find({ email: req.body.email.toLowerCase(), active: 1 }).exec();
            if (isRecuriterExist.length == 0) {
                let mailRes = await sendEmail(req.body.email, req.body.rname);
                if (mailRes.status == 200) {
                    req.body.otp = {
                        otp: mailRes.OTP,
                        tms: Date.now()
                    };
                    let domainSplit = req.body.email.split("@");
                    let isDomainExits = await companySchema.findOne({ domain: domainSplit[1] }).exec();
                    if (!isDomainExits) {
                        var newCompany = new companySchema({
                            cname: body.cname,
                            domain: domainSplit[1],
                            caddr: body.caddr,
                            city: body.city,
                            cdate: new Date()
                        });
                        let companyDetails = await newCompany.save();
                        req.body.cid = companyDetails._id;
                    }
                    else {
                        req.body.cid = isDomainExits._id;
                    }

                    insertRecuriter(req.body, res)
                }
                else {
                    res.send(new APIError(201, OtpSendToMailMsg))
                }
            }
            else {
                res.send(new APIError(201, userExistsMsg))
            }
        }

    } catch (error) {
        res.send(new APIError(201, error))
    }
});

router.post("/addRecruiter", async (req, res) => {
    try {
        let body = req.body;
        if (!body.id) {
            res.send(new APIError(201, idMsg))
        }
        else if (!body.rname.trim()) {
            res.send(new APIError(201, nameMsg))
        }
        else if (!body.email) {
            res.send(new APIError(201, emailMsg))
        }
        else if (emailValidation(body.email)) {
            res.send(new APIError(201, emailErrMsg))
        }
        else if (!body.con) {
            res.send(new APIError(201, contactNumberMsg))
        }
        else if (!body.cname.trim()) {
            res.send(new APIError(201, companyNameMsg))
        }
        else if (!body.city) {
            res.send(new APIError(201, cityMsg))
        }
        else {
            let isId: any = await invitationSchema.findOne({ _id: mongoose.Types.ObjectId(body.id), toemail: body.email.toLowerCase() }).exec();
            if (isId) {
                if (isId.isregister == 0) {
                    let company = await companySchema.findOne({ cname: body.cname }).exec();
                    let isRecExists = await recuriterSchema.findOne({ email: body.email.toLowerCase() }).exec();
                    if (!company) {
                        if (!isRecExists) {
                            let mailRes = await sendEmail(req.body.email, req.body.rname);
                            if (mailRes.status == 200) {
                                req.body.otp = {
                                    otp: mailRes.OTP,
                                    tms: Date.now()
                                };
                                var newCompany = new companySchema({
                                    cname: body.cname,
                                    caddr: body.caddr,
                                    city: body.city,
                                    crdt: new Date()
                                });
                                let companyDetails = await newCompany.save();
                                req.body.cid = companyDetails._id;
                                req.body.isprimary = 1;
                                await invitationSchema.updateOne({
                                    _id: mongoose.Types.ObjectId(body.id),
                                }, {
                                    $set: {
                                        isregister: 1,
                                        registerdt: new Date().toISOString(),
                                        cid: req.body.cid
                                    }
                                });
                                insertRecuriter(req.body, res)
                            }
                            else {
                                res.send(new APIError(201, OtpSendToMailMsg))
                            }
                        }
                        else {
                            res.send(new APIError(201, recuriterExistMsg));
                        }
                    }
                    else {
                        res.send(new APIError(201, companyExistsMsg))
                    }
                }
                else {
                    res.send(new APIError(201, companyExistsMsg))
                }
            }
            else {
                res.send(new APIError(201, invalidDetailsMsg))
            }
        }

    } catch (error) {
        res.send(new APIError(201, error))
    }
});

async function insertRecuriter(body, res) {
    body.email = body.email.toLowerCase();
    let newRecruiter = new recuriterSchema(body)
    let recruiterDetails = await newRecruiter.save();
    if (recruiterDetails) {
        //let mailRes = await recSuccessEmail(body.email);
        //if (mailRes.status == 200) {
            res.send(new Status(200, { rid: recruiterDetails._id }));
        //}
        // else {
        //     res.send(new APIError(201, emailErrorMsg))
        // }
    }
}


router.post('/otpVerify', async (req, res, next) => {
    try {
        let body = req.body;
        // if (!body.rid) {
        //     res.send(new APIError(201, recuriterIdMsg))
        // }
        if (!body.email) {
            res.send(new APIError(201, emailMsg))
        }
        else if (!body.otp) {
            res.send(new APIError(201, OtpMsg))
        }

        else {
            let Email = body.email.toLowerCase();
            let user: any = await recuriterSchema.findOne({ email: Email }).exec();
            if (user) {
                if (body.otp.trim() == user.otp.otp) {
                    let currentTime = new Date();
                    let cMt = currentTime.getHours() * 60 + currentTime.getMinutes();
                    let otpTime = new Date(parseInt(user.otp.tms));
                    let oMt = otpTime.getHours() * 60 + otpTime.getMinutes();
                    let Diff = cMt - oMt;
                    if (Diff < OtpExpTime) {
                        delete user._doc.otp;
                        res.send(new Status(200, user))
                    }
                    else {
                        res.send(new APIError(201, OtpExpireMsg))
                    }
                }
                else {
                    res.send(new APIError(201, invalidOtpMsg))
                }
            }
            else {
                res.send(new APIError(201, userNotExistsMsg))
            }
        }
    } catch (error) {
        res.send(new APIError(201, error))
    }

})

router.post('/resendOTP', async (req, res, next) => {
    try {
        let body = req.body;
        if (!body.email) {
            res.send(new APIError(201, emailMsg))
        }
        else {
            req.body.email = req.body.email.toLowerCase();
            let isRecuriterExist: any = await recuriterSchema.find({ email: req.body.email }).exec();
            if (isRecuriterExist.length > 0) {
                let mailRes = await sendEmail(req.body.email, isRecuriterExist[0].rname);
                isRecuriterExist.otp = {
                    otp: mailRes.OTP,
                    tms: Date.now()
                };
                await recuriterSchema.updateOne({ email: req.body.email }, { $set: { otp: isRecuriterExist.otp } });
                res.send(new Status(200, { rid: isRecuriterExist[0]._id }))
            }
            else {
                res.send(new APIError(201, userNotExistsMsg))
            }
        }
    } catch (error) {
        res.send(new APIError(201, error))
    }

})

export =router;


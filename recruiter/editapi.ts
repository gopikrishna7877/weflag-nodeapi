import { Router } from 'express';
import { APIError } from '../utils/APIError';
import { recuriterSchema } from '../modals/recruiters';
import { recuriterIdMsg, recuriterNotExistMsg, nameMsg, emailMsg, emailErrMsg, contactNumberMsg, companyNameMsg, cityMsg } from '../utils/errorMsg';
import { Status } from '../utils/status';
import { emailValidation } from '../utils/email';
import * as mongoose from 'mongoose';
let router = Router();

router.post("/getRecruiter", async (req, res, next) => {
    try {
        let body = req.body;
        if (!body.rid.trim()) {
            res.send(new APIError(201, recuriterIdMsg))
        }
        else {
            let recuriter = await recuriterSchema.findOne({ "_id": mongoose.Types.ObjectId(body.rid), active: 1 }).exec();
            if (recuriter) {
                res.send(new Status(200, recuriter))
            }
            else {
                res.send(new APIError(201, recuriterNotExistMsg))
            }
        }
    } catch (error) {
        res.send(new APIError(201, error));
    }
})

router.post("/updateRecruiter", async (req, res, next) => {
    try {
        let body = req.body;
        if (!body.rname.trim()) {
            res.send(new APIError(201, nameMsg))
        }
        else if (!body.rid) {
            res.send(new APIError(201, recuriterIdMsg))
        }
        else if (!body.email) {
            res.send(new APIError(201, emailMsg))
        }
        else if (emailValidation(body.email)) {
            res.send(new APIError(201, emailErrMsg))
        }
        else if (!body.con) {
            res.send(new APIError(201, contactNumberMsg))
        }
        else if (!body.cname.trim()) {
            res.send(new APIError(201, companyNameMsg))
        }
        else if (!body.city) {
            res.send(new APIError(201, cityMsg))
        }
        else {
            let recruiter = await recuriterSchema.findOneAndUpdate(
                {
                    _id: mongoose.Types.ObjectId(body.rid)
                },
                {
                    $set: {
                        rname: body.rname,
                        city: body.city,
                        con: body.con,
                        caddr: body.caddr
                    }
                }).exec();
            let getrecuriter = await recuriterSchema.findOne({ "_id": mongoose.Types.ObjectId(body.rid), active: 1 }).exec();
            res.send(new Status(200, getrecuriter));
        }
    } catch (error) {
        res.send(new APIError(201, error))
    }
})
 

export =router;
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var express_1 = require("express");
var router = express_1.Router();
var status_1 = require("../utils/status");
var APIError_1 = require("../utils/APIError");
var email_1 = require("../utils/email");
var recruiters_1 = require("../modals/recruiters");
var company_1 = require("../modals/company");
var mongoose = require("mongoose");
var errorMsg_1 = require("../utils/errorMsg");
var invitation_1 = require("../modals/invitation");
router.post("/_addRecruiter", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var body, isRecuriterExist, mailRes, domainSplit, isDomainExits, newCompany, companyDetails, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 17, , 18]);
                body = req.body;
                if (!!body.rname.trim()) return [3 /*break*/, 1];
                res.send(new APIError_1.APIError(201, errorMsg_1.nameMsg));
                return [3 /*break*/, 16];
            case 1:
                if (!!body.email) return [3 /*break*/, 2];
                res.send(new APIError_1.APIError(201, errorMsg_1.emailMsg));
                return [3 /*break*/, 16];
            case 2:
                if (!email_1.emailValidation(body.email)) return [3 /*break*/, 3];
                res.send(new APIError_1.APIError(201, errorMsg_1.emailErrMsg));
                return [3 /*break*/, 16];
            case 3:
                if (!!body.con) return [3 /*break*/, 4];
                res.send(new APIError_1.APIError(201, errorMsg_1.contactNumberMsg));
                return [3 /*break*/, 16];
            case 4:
                if (!!body.cname.trim()) return [3 /*break*/, 5];
                res.send(new APIError_1.APIError(201, errorMsg_1.companyNameMsg));
                return [3 /*break*/, 16];
            case 5:
                if (!!body.city) return [3 /*break*/, 6];
                res.send(new APIError_1.APIError(201, errorMsg_1.cityMsg));
                return [3 /*break*/, 16];
            case 6: return [4 /*yield*/, recruiters_1.recuriterSchema.find({ email: req.body.email.toLowerCase(), active: 1 }).exec()];
            case 7:
                isRecuriterExist = _a.sent();
                if (!(isRecuriterExist.length == 0)) return [3 /*break*/, 15];
                return [4 /*yield*/, email_1.sendEmail(req.body.email, req.body.rname)];
            case 8:
                mailRes = _a.sent();
                if (!(mailRes.status == 200)) return [3 /*break*/, 13];
                req.body.otp = {
                    otp: mailRes.OTP,
                    tms: Date.now()
                };
                domainSplit = req.body.email.split("@");
                return [4 /*yield*/, company_1.companySchema.findOne({ domain: domainSplit[1] }).exec()];
            case 9:
                isDomainExits = _a.sent();
                if (!!isDomainExits) return [3 /*break*/, 11];
                newCompany = new company_1.companySchema({
                    cname: body.cname,
                    domain: domainSplit[1],
                    caddr: body.caddr,
                    city: body.city,
                    cdate: new Date()
                });
                return [4 /*yield*/, newCompany.save()];
            case 10:
                companyDetails = _a.sent();
                req.body.cid = companyDetails._id;
                return [3 /*break*/, 12];
            case 11:
                req.body.cid = isDomainExits._id;
                _a.label = 12;
            case 12:
                insertRecuriter(req.body, res);
                return [3 /*break*/, 14];
            case 13:
                res.send(new APIError_1.APIError(201, errorMsg_1.OtpSendToMailMsg));
                _a.label = 14;
            case 14: return [3 /*break*/, 16];
            case 15:
                res.send(new APIError_1.APIError(201, errorMsg_1.userExistsMsg));
                _a.label = 16;
            case 16: return [3 /*break*/, 18];
            case 17:
                error_1 = _a.sent();
                res.send(new APIError_1.APIError(201, error_1));
                return [3 /*break*/, 18];
            case 18: return [2 /*return*/];
        }
    });
}); });
router.post("/addRecruiter", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var body, isId, company, isRecExists, mailRes, newCompany, companyDetails, error_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 24, , 25]);
                body = req.body;
                if (!!body.id) return [3 /*break*/, 1];
                res.send(new APIError_1.APIError(201, errorMsg_1.idMsg));
                return [3 /*break*/, 23];
            case 1:
                if (!!body.rname.trim()) return [3 /*break*/, 2];
                res.send(new APIError_1.APIError(201, errorMsg_1.nameMsg));
                return [3 /*break*/, 23];
            case 2:
                if (!!body.email) return [3 /*break*/, 3];
                res.send(new APIError_1.APIError(201, errorMsg_1.emailMsg));
                return [3 /*break*/, 23];
            case 3:
                if (!email_1.emailValidation(body.email)) return [3 /*break*/, 4];
                res.send(new APIError_1.APIError(201, errorMsg_1.emailErrMsg));
                return [3 /*break*/, 23];
            case 4:
                if (!!body.con) return [3 /*break*/, 5];
                res.send(new APIError_1.APIError(201, errorMsg_1.contactNumberMsg));
                return [3 /*break*/, 23];
            case 5:
                if (!!body.cname.trim()) return [3 /*break*/, 6];
                res.send(new APIError_1.APIError(201, errorMsg_1.companyNameMsg));
                return [3 /*break*/, 23];
            case 6:
                if (!!body.city) return [3 /*break*/, 7];
                res.send(new APIError_1.APIError(201, errorMsg_1.cityMsg));
                return [3 /*break*/, 23];
            case 7: return [4 /*yield*/, invitation_1.invitationSchema.findOne({ _id: mongoose.Types.ObjectId(body.id), toemail: body.email.toLowerCase() }).exec()];
            case 8:
                isId = _a.sent();
                if (!isId) return [3 /*break*/, 22];
                if (!(isId.isregister == 0)) return [3 /*break*/, 20];
                return [4 /*yield*/, company_1.companySchema.findOne({ cname: body.cname }).exec()];
            case 9:
                company = _a.sent();
                return [4 /*yield*/, recruiters_1.recuriterSchema.findOne({ email: body.email.toLowerCase() }).exec()];
            case 10:
                isRecExists = _a.sent();
                if (!!company) return [3 /*break*/, 18];
                if (!!isRecExists) return [3 /*break*/, 16];
                return [4 /*yield*/, email_1.sendEmail(req.body.email, req.body.rname)];
            case 11:
                mailRes = _a.sent();
                if (!(mailRes.status == 200)) return [3 /*break*/, 14];
                req.body.otp = {
                    otp: mailRes.OTP,
                    tms: Date.now()
                };
                newCompany = new company_1.companySchema({
                    cname: body.cname,
                    caddr: body.caddr,
                    city: body.city,
                    crdt: new Date()
                });
                return [4 /*yield*/, newCompany.save()];
            case 12:
                companyDetails = _a.sent();
                req.body.cid = companyDetails._id;
                req.body.isprimary = 1;
                return [4 /*yield*/, invitation_1.invitationSchema.updateOne({
                        _id: mongoose.Types.ObjectId(body.id)
                    }, {
                        $set: {
                            isregister: 1,
                            registerdt: new Date().toISOString(),
                            cid: req.body.cid
                        }
                    })];
            case 13:
                _a.sent();
                insertRecuriter(req.body, res);
                return [3 /*break*/, 15];
            case 14:
                res.send(new APIError_1.APIError(201, errorMsg_1.OtpSendToMailMsg));
                _a.label = 15;
            case 15: return [3 /*break*/, 17];
            case 16:
                res.send(new APIError_1.APIError(201, errorMsg_1.recuriterExistMsg));
                _a.label = 17;
            case 17: return [3 /*break*/, 19];
            case 18:
                res.send(new APIError_1.APIError(201, errorMsg_1.companyExistsMsg));
                _a.label = 19;
            case 19: return [3 /*break*/, 21];
            case 20:
                res.send(new APIError_1.APIError(201, errorMsg_1.companyExistsMsg));
                _a.label = 21;
            case 21: return [3 /*break*/, 23];
            case 22:
                res.send(new APIError_1.APIError(201, errorMsg_1.invalidDetailsMsg));
                _a.label = 23;
            case 23: return [3 /*break*/, 25];
            case 24:
                error_2 = _a.sent();
                res.send(new APIError_1.APIError(201, error_2));
                return [3 /*break*/, 25];
            case 25: return [2 /*return*/];
        }
    });
}); });
function insertRecuriter(body, res) {
    return __awaiter(this, void 0, void 0, function () {
        var newRecruiter, recruiterDetails;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    body.email = body.email.toLowerCase();
                    newRecruiter = new recruiters_1.recuriterSchema(body);
                    return [4 /*yield*/, newRecruiter.save()];
                case 1:
                    recruiterDetails = _a.sent();
                    if (recruiterDetails) {
                        //let mailRes = await recSuccessEmail(body.email);
                        //if (mailRes.status == 200) {
                        res.send(new status_1.Status(200, { rid: recruiterDetails._id }));
                        //}
                        // else {
                        //     res.send(new APIError(201, emailErrorMsg))
                        // }
                    }
                    return [2 /*return*/];
            }
        });
    });
}
router.post('/otpVerify', function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var body, Email, user, currentTime, cMt, otpTime, oMt, Diff, error_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 5, , 6]);
                body = req.body;
                if (!!body.email) return [3 /*break*/, 1];
                res.send(new APIError_1.APIError(201, errorMsg_1.emailMsg));
                return [3 /*break*/, 4];
            case 1:
                if (!!body.otp) return [3 /*break*/, 2];
                res.send(new APIError_1.APIError(201, errorMsg_1.OtpMsg));
                return [3 /*break*/, 4];
            case 2:
                Email = body.email.toLowerCase();
                return [4 /*yield*/, recruiters_1.recuriterSchema.findOne({ email: Email }).exec()];
            case 3:
                user = _a.sent();
                if (user) {
                    if (body.otp.trim() == user.otp.otp) {
                        currentTime = new Date();
                        cMt = currentTime.getHours() * 60 + currentTime.getMinutes();
                        otpTime = new Date(parseInt(user.otp.tms));
                        oMt = otpTime.getHours() * 60 + otpTime.getMinutes();
                        Diff = cMt - oMt;
                        if (Diff < errorMsg_1.OtpExpTime) {
                            delete user._doc.otp;
                            res.send(new status_1.Status(200, user));
                        }
                        else {
                            res.send(new APIError_1.APIError(201, errorMsg_1.OtpExpireMsg));
                        }
                    }
                    else {
                        res.send(new APIError_1.APIError(201, errorMsg_1.invalidOtpMsg));
                    }
                }
                else {
                    res.send(new APIError_1.APIError(201, errorMsg_1.userNotExistsMsg));
                }
                _a.label = 4;
            case 4: return [3 /*break*/, 6];
            case 5:
                error_3 = _a.sent();
                res.send(new APIError_1.APIError(201, error_3));
                return [3 /*break*/, 6];
            case 6: return [2 /*return*/];
        }
    });
}); });
router.post('/resendOTP', function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var body, isRecuriterExist, mailRes, error_4;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 7, , 8]);
                body = req.body;
                if (!!body.email) return [3 /*break*/, 1];
                res.send(new APIError_1.APIError(201, errorMsg_1.emailMsg));
                return [3 /*break*/, 6];
            case 1:
                req.body.email = req.body.email.toLowerCase();
                return [4 /*yield*/, recruiters_1.recuriterSchema.find({ email: req.body.email }).exec()];
            case 2:
                isRecuriterExist = _a.sent();
                if (!(isRecuriterExist.length > 0)) return [3 /*break*/, 5];
                return [4 /*yield*/, email_1.sendEmail(req.body.email, isRecuriterExist[0].rname)];
            case 3:
                mailRes = _a.sent();
                isRecuriterExist.otp = {
                    otp: mailRes.OTP,
                    tms: Date.now()
                };
                return [4 /*yield*/, recruiters_1.recuriterSchema.updateOne({ email: req.body.email }, { $set: { otp: isRecuriterExist.otp } })];
            case 4:
                _a.sent();
                res.send(new status_1.Status(200, { rid: isRecuriterExist[0]._id }));
                return [3 /*break*/, 6];
            case 5:
                res.send(new APIError_1.APIError(201, errorMsg_1.userNotExistsMsg));
                _a.label = 6;
            case 6: return [3 /*break*/, 8];
            case 7:
                error_4 = _a.sent();
                res.send(new APIError_1.APIError(201, error_4));
                return [3 /*break*/, 8];
            case 8: return [2 /*return*/];
        }
    });
}); });
module.exports = router;

import { Router } from 'express';
let router = Router();
import * as mongoose from 'mongoose';
import { Status } from '../utils/status';
import { APIError } from '../utils/APIError';
var db = mongoose.connection;
//var citiesCollection = db.collection('city');
import { citiesSchema } from '../modals/cities';
import { errorMsg, noDataMsg } from '../utils/errorMsg';

router.post('/getCities', async (req, res) => {
    try {
        let citylist = await citiesSchema.find({}).exec();
        if (citylist) {
            res.send(new Status(200, citylist));
        } else {
            res.send(new APIError(201, noDataMsg))
        }
    }
    catch (err) {
        res.send(new APIError(201, errorMsg))
    }
})

export = router;
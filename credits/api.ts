import { Router } from 'express';
import * as mongoose from 'mongoose';
import { APIError } from '../utils/APIError';
import { companyIdMsg, creditsIdMsg, noShowValueMsg, notJoinedValueMsg, joinedValueMsg, searchValueMsg } from '../utils/errorMsg';
import { transtionCreditsSchema } from '../modals/walletcredits';
import { Status } from '../utils/status';
import { crLookSchema } from '../modals/creditslookup';
let router = Router();

let body;
router.post("/companyTrnCreditsList", async (req, res, next) => {
    try {
        body = req.body;
        if (!body.cid) {
            res.send(new APIError(201, companyIdMsg))
        }
        else {
            let trnsList = await transtionCreditsSchema.find({ cid: mongoose.Types.ObjectId(body.cid) }).sort({ "createdAt": -1 }).exec();
            res.send(new Status(200, trnsList));
        }

    } catch (error) {
        res.send(new APIError(201, error));
    }
});
router.post("/recruiterTrnCreditsList", async (req, res, next) => {
    try {
        body = req.body;
        if (!body.rid) {
            res.send(new APIError(201, companyIdMsg))
        }
        else {
            let trnsList = await transtionCreditsSchema.find({ rid: mongoose.Types.ObjectId(body.rid) }).sort({ "createdAt": -1 }).exec();
            res.send(new Status(200, trnsList));
        }

    } catch (error) {
        res.send(new APIError(201, error));
    }
});

router.post("/getCreditsLookup", async (req, res, next) => {
    try {
        let cLk = await crLookSchema.find({}).exec();
        res.send(new Status(200, cLk))

    } catch (error) {
        res.send(new APIError(201, error));
    }
});

router.post("/updateCreditsLookup", async (req, res, next) => {
    try {
        body = req.body;
        if (!body.id) {
            res.send(new APIError(201, creditsIdMsg))
        }
        else if (!body.noshow) {
            res.send(new APIError(201, noShowValueMsg))
        }
        else if (!body.notjoined) {
            res.send(new APIError(201, notJoinedValueMsg))
        }
        else if (!body.joined) {
            res.send(new APIError(201, joinedValueMsg))
        }
        else if (!body.search) {
            res.send(new APIError(201, searchValueMsg))
        }
        else {

            let newCrdts = await crLookSchema.updateOne(
                { _id: mongoose.Types.ObjectId(body.id) },
                {
                    $set: {
                        noshow: body.noshow,
                        notjoined: body.notjoined,
                        joined: body.joined,
                        search: body.search,
                        updt: new Date().toISOString()
                    }
                }
            ).exec();
            let newCl = await crLookSchema.findOne({ _id: mongoose.Types.ObjectId(body.id) }).exec();
            res.send(new Status(200, newCl));
        }

    } catch (error) {
        res.send(new APIError(201, error));
    }
})

export =router;

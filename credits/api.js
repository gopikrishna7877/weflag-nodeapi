"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var express_1 = require("express");
var mongoose = require("mongoose");
var APIError_1 = require("../utils/APIError");
var errorMsg_1 = require("../utils/errorMsg");
var walletcredits_1 = require("../modals/walletcredits");
var status_1 = require("../utils/status");
var creditslookup_1 = require("../modals/creditslookup");
var router = express_1.Router();
var body;
router.post("/companyTrnCreditsList", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var trnsList, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 4, , 5]);
                body = req.body;
                if (!!body.cid) return [3 /*break*/, 1];
                res.send(new APIError_1.APIError(201, errorMsg_1.companyIdMsg));
                return [3 /*break*/, 3];
            case 1: return [4 /*yield*/, walletcredits_1.transtionCreditsSchema.find({ cid: mongoose.Types.ObjectId(body.cid) }).sort({ "createdAt": -1 }).exec()];
            case 2:
                trnsList = _a.sent();
                res.send(new status_1.Status(200, trnsList));
                _a.label = 3;
            case 3: return [3 /*break*/, 5];
            case 4:
                error_1 = _a.sent();
                res.send(new APIError_1.APIError(201, error_1));
                return [3 /*break*/, 5];
            case 5: return [2 /*return*/];
        }
    });
}); });
router.post("/recruiterTrnCreditsList", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var trnsList, error_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 4, , 5]);
                body = req.body;
                if (!!body.rid) return [3 /*break*/, 1];
                res.send(new APIError_1.APIError(201, errorMsg_1.companyIdMsg));
                return [3 /*break*/, 3];
            case 1: return [4 /*yield*/, walletcredits_1.transtionCreditsSchema.find({ rid: mongoose.Types.ObjectId(body.rid) }).sort({ "createdAt": -1 }).exec()];
            case 2:
                trnsList = _a.sent();
                res.send(new status_1.Status(200, trnsList));
                _a.label = 3;
            case 3: return [3 /*break*/, 5];
            case 4:
                error_2 = _a.sent();
                res.send(new APIError_1.APIError(201, error_2));
                return [3 /*break*/, 5];
            case 5: return [2 /*return*/];
        }
    });
}); });
router.post("/getCreditsLookup", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var cLk, error_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, creditslookup_1.crLookSchema.find({}).exec()];
            case 1:
                cLk = _a.sent();
                res.send(new status_1.Status(200, cLk));
                return [3 /*break*/, 3];
            case 2:
                error_3 = _a.sent();
                res.send(new APIError_1.APIError(201, error_3));
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
router.post("/updateCreditsLookup", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var newCrdts, newCl, error_4;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 9, , 10]);
                body = req.body;
                if (!!body.id) return [3 /*break*/, 1];
                res.send(new APIError_1.APIError(201, errorMsg_1.creditsIdMsg));
                return [3 /*break*/, 8];
            case 1:
                if (!!body.noshow) return [3 /*break*/, 2];
                res.send(new APIError_1.APIError(201, errorMsg_1.noShowValueMsg));
                return [3 /*break*/, 8];
            case 2:
                if (!!body.notjoined) return [3 /*break*/, 3];
                res.send(new APIError_1.APIError(201, errorMsg_1.notJoinedValueMsg));
                return [3 /*break*/, 8];
            case 3:
                if (!!body.joined) return [3 /*break*/, 4];
                res.send(new APIError_1.APIError(201, errorMsg_1.joinedValueMsg));
                return [3 /*break*/, 8];
            case 4:
                if (!!body.search) return [3 /*break*/, 5];
                res.send(new APIError_1.APIError(201, errorMsg_1.searchValueMsg));
                return [3 /*break*/, 8];
            case 5: return [4 /*yield*/, creditslookup_1.crLookSchema.updateOne({ _id: mongoose.Types.ObjectId(body.id) }, {
                    $set: {
                        noshow: body.noshow,
                        notjoined: body.notjoined,
                        joined: body.joined,
                        search: body.search,
                        updt: new Date().toISOString()
                    }
                }).exec()];
            case 6:
                newCrdts = _a.sent();
                return [4 /*yield*/, creditslookup_1.crLookSchema.findOne({ _id: mongoose.Types.ObjectId(body.id) }).exec()];
            case 7:
                newCl = _a.sent();
                res.send(new status_1.Status(200, newCl));
                _a.label = 8;
            case 8: return [3 /*break*/, 10];
            case 9:
                error_4 = _a.sent();
                res.send(new APIError_1.APIError(201, error_4));
                return [3 /*break*/, 10];
            case 10: return [2 /*return*/];
        }
    });
}); });
module.exports = router;

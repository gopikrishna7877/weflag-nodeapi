"use strict";
var express = require("express");
var bodyParser = require("body-parser");
var jobSeekerRoute = require("./jobseeker/api");
var dashBoardRoute = require("./dashboard/api");
var recuriterRoute = require("./recruiter/editapi");
var creditsRoute = require("./credits/api");
var adminRoute = require("./adminAPI/api");
var app = express();
app.use(bodyParser.json());
// app.use(async (req, res, next) => {
//     let apiKey;
//     if (req.headers && req.headers.authorization) {
//         const parts = req.headers.authorization.split(' ');
//         if (parts.length === 2 && parts[0] === 'Bearer') {
//             apiKey = parts[1];
//         }
//     }
//     if (apiKey) {
//         verify(apiKey, JWT_SECRET, async (error, decoded) => {
//             if (error) {
//                 return res.send(error.message);
//             }
//             //  req.token = decoded.user;
//             next();
//         })
//     }
//     else {
//         return res.send("This access token is not valid anymore. Please login.", 401);
//     }
// })
app.use('/jobSeeker', jobSeekerRoute);
app.use('/credits', creditsRoute);
app.use('/dashBoard', dashBoardRoute);
app.use('/recuriter', recuriterRoute);
app.use('/admin', adminRoute);
module.exports = app;

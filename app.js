"use strict";
var express = require("express");
var http_status_codes_1 = require("http-status-codes");
var app = express();
var mongoose = require("mongoose");
var logger = require("morgan");
var crypt_1 = require("./utils/crypt");
var bodyParser = require("body-parser");
var cors = require("cors");
var apiRouter = require("./api");
var loginRouter = require("./login/api");
var recruiterRouter = require("./recruiter/api");
var citiesRouter = require("./city/api");
var adminRouter = require("./adminAPI/login");
app.use(bodyParser.json());
//Set up default mongoose connection
//var mongoDB = 'mongodb://localhost:27017/WeFlag';
// const encryptedString ="mongodb://Asset_weflag:H^ppy^24@182.18.139.99:27017/weflag" //'mongodb://182.18.139.99:27017/weflag';
// const en=encrypt(encryptedString);
// console.log(en + "Naresh k")
//console.log(encryptedString +'Naresh')
var encryptedString = crypt_1.decrypt('8099385fa55f6d0dcc7fa5f4d8b4c6dc6d96d66d6abd2860dbd602d2693a271515d72b358f9614e31789ff42d85a5d8d81b77202755b855e3c5a82d2255554846147eafcb4bdf35debb6eba2884d3222f44fe1babb2fbc811275af973560c74af5fcc72be924d00d10dd593d3150e8bd3722cacc46322953e7304546ff59304c56470d5a864e6432459667106b229c49ac0f91ac2a4ba68fc6a2');
// console.log(mongoDB +'db name')
mongoose.connect(encryptedString, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});
//Get the default connection
var db = mongoose.connection;
//db error
db.on('error', console.error.bind(console, 'MongoDB connection error..'));
app.use(logger('dev'));
app.use(cors());
app.get('/', function (req, res) {
    res.status(http_status_codes_1.OK).send("WeFlag.....");
});
app.use('/login', loginRouter);
app.use('/api', apiRouter);
app.use('/recruiter', recruiterRouter);
app.use('/city', citiesRouter);
app.use('/admin', adminRouter);
app.use(function (req, res, next) {
    res.status(400);
    res.send({ error: 'Not found' });
    return;
});
module.exports = app;

"use strict";
exports.__esModule = true;
var app = require("./app");
var PORT = 3001;
app.listen(PORT, function () {
    console.log("Listening on port " + PORT);
});

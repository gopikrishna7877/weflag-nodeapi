"use strict";
exports.__esModule = true;
var Status = /** @class */ (function () {
    function Status(code, data) {
        if (code === void 0) { code = 200; }
        this.status = code,
            this.data = data;
    }
    return Status;
}());
exports.Status = Status;

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var mail = require("nodemailer");
var errorMsg_1 = require("./errorMsg");
var OTP = '';
var link = "http://45.249.111.167/WeFlag/#/signup";
var loginLink = "http://45.249.111.167/WeFlag/#/login";
function generateOTP() {
    var digits = "0123456789";
    for (var i = 0; i < 4; i++) {
        OTP += digits[Math.floor(Math.random() * 10)];
    }
    return OTP;
}
var trans = mail.createTransport({
    host: 'smtp.sendgrid.net',
    port: 587,
    secure: false,
    auth: {
        user: 'bhanu.asset',
        pass: 'Bhanuatpl@123'
    }
});
function sendEmail(toEmail, rname) {
    return __awaiter(this, void 0, void 0, function () {
        var mailOpt, response;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    OTP = '';
                    mailOpt = {
                        from: 'noreply.weflag@gmail.com',
                        to: toEmail,
                        subject: 'WeFlag - OTP verification',
                        html: 'Dear ' + rname + ', <br><br> Greetings from WeFlag Team! <br><br><br>Use OTP ' + generateOTP() + ' to verify your WeFlag account. <br>This OTP is valid for next 5 minutes only. <br><br><br>Thanks,<br>WeFlag Team.',
                        otp: OTP
                    };
                    return [4 /*yield*/, trans.sendMail(mailOpt)];
                case 1:
                    response = _a.sent();
                    if (response.messageId) {
                        response.status = 200;
                        response.OTP = mailOpt.otp;
                    }
                    else {
                        response.status = 400;
                    }
                    return [2 /*return*/, response];
            }
        });
    });
}
exports.sendEmail = sendEmail;
function sendInvitaionEmail(toEmail, id) {
    return __awaiter(this, void 0, void 0, function () {
        var mailOpt, response;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    mailOpt = {
                        from: 'noreply.weflag@gmail.com',
                        to: toEmail,
                        subject: 'WeFlag - Invitation',
                        html: 'Dear Member, <br><br> Greetings from WeFlag Team! <br><br><br>Use link ' + link + "/" + id + ' to signup WeFlag account. <br><br>Thanks,<br>WeFlag Team.'
                    };
                    return [4 /*yield*/, trans.sendMail(mailOpt)];
                case 1:
                    response = _a.sent();
                    if (response.messageId) {
                        response.status = 200;
                    }
                    else {
                        response.status = 400;
                    }
                    return [2 /*return*/, response];
            }
        });
    });
}
exports.sendInvitaionEmail = sendInvitaionEmail;
function recSuccessEmail(toEmail) {
    return __awaiter(this, void 0, void 0, function () {
        var mailOpt, response;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    mailOpt = {
                        from: 'noreply.weflag@gmail.com',
                        to: toEmail,
                        subject: 'WeFlag - Login',
                        html: 'Dear Member, <br><br> Greetings from WeFlag Team! <br><br><br>Use link ' + loginLink + ' to signin WeFlag account. <br><br>Thanks,<br>WeFlag Team.'
                    };
                    return [4 /*yield*/, trans.sendMail(mailOpt)];
                case 1:
                    response = _a.sent();
                    if (response.messageId) {
                        response.status = 200;
                    }
                    else {
                        response.status = 400;
                    }
                    return [2 /*return*/, response];
            }
        });
    });
}
exports.recSuccessEmail = recSuccessEmail;
function activeOrInactiveEmail(toEmail, status, text) {
    return __awaiter(this, void 0, void 0, function () {
        var mailOpt, response;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    mailOpt = {
                        from: 'noreply.weflag@gmail.com',
                        to: toEmail,
                        subject: 'WeFlag - Account ' + status,
                        html: 'Dear Member, <br><br> Greetings from WeFlag Team! <br><br><br>Your account has been ' + status + '' + text + '. <br><br>Thanks,<br>WeFlag Team.'
                    };
                    return [4 /*yield*/, trans.sendMail(mailOpt)];
                case 1:
                    response = _a.sent();
                    if (response.messageId) {
                        response.status = 200;
                    }
                    else {
                        response.status = 400;
                    }
                    return [2 /*return*/, response];
            }
        });
    });
}
exports.activeOrInactiveEmail = activeOrInactiveEmail;
function emailValidation(email) {
    var emailSplit = email.toLowerCase().split("@");
    var s = "@" + emailSplit[1];
    if (errorMsg_1.freeEmailList.includes(s)) {
        return true;
    }
    else {
        return false;
    }
}
exports.emailValidation = emailValidation;

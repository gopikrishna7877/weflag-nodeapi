"use strict";
exports.__esModule = true;
var encryp = require("cryptr");
var cryptr = new encryp('123abc242526xyz');
function encrypt(text) {
    return cryptr.encrypt(text);
}
exports.encrypt = encrypt;
function decrypt(text) {
    return cryptr.decrypt(text);
}
exports.decrypt = decrypt;

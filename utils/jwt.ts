import { verify, sign } from 'jsonwebtoken';
export const JWT_SECRET = 'abc_123_xyz_242526';
const jwtExpirySeconds = 60;

export async function generateToken(user) {
    try {
        const token = await sign({ username: user._id }, JWT_SECRET, {
            algorithm: 'HS256',
            expiresIn: jwtExpirySeconds
        });
        return token;
    } catch (error) {
        return error
    }
}


import * as mail from 'nodemailer';
import { freeEmailList } from './errorMsg';
let OTP = '';
let link = "http://45.249.111.167/WeFlag/#/signup";
let loginLink = "http://45.249.111.167/WeFlag/#/login"
function generateOTP() {
    var digits = "0123456789";

    for (let i = 0; i < 4; i++) {
        OTP += digits[Math.floor(Math.random() * 10)];
    }
    return OTP;
}
var trans = mail.createTransport({
    host: 'smtp.sendgrid.net',
    port: 587,
    secure: false,
    auth: {
        user: 'bhanu.asset',
        pass: 'Bhanuatpl@123'
    }
})
export async function sendEmail(toEmail, rname) {
    OTP = '';
    var mailOpt = {
        from: 'noreply.weflag@gmail.com',
        to: toEmail,
        subject: 'WeFlag - OTP verification',
        html: 'Dear ' + rname + ', <br><br> Greetings from WeFlag Team! <br><br><br>Use OTP ' + generateOTP() + ' to verify your WeFlag account. <br>This OTP is valid for next 5 minutes only. <br><br><br>Thanks,<br>WeFlag Team.',
        otp: OTP
    }
    let response;
    response = await trans.sendMail(mailOpt);
    if (response.messageId) {
        response.status = 200;
        response.OTP = mailOpt.otp;
    }
    else {
        response.status = 400;
    }
    return response;
}

export async function sendInvitaionEmail(toEmail, id) {
    var mailOpt = {
        from: 'noreply.weflag@gmail.com',
        to: toEmail,
        subject: 'WeFlag - Invitation',
        html: 'Dear Member, <br><br> Greetings from WeFlag Team! <br><br><br>Use link ' + link + "/" + id + ' to signup WeFlag account. <br><br>Thanks,<br>WeFlag Team.'
    }
    let response;
    response = await trans.sendMail(mailOpt);
    if (response.messageId) {
        response.status = 200;
    }
    else {
        response.status = 400;
    }
    return response;
}
export async function recSuccessEmail(toEmail) {
    var mailOpt = {
        from: 'noreply.weflag@gmail.com',
        to: toEmail,
        subject: 'WeFlag - Login',
        html: 'Dear Member, <br><br> Greetings from WeFlag Team! <br><br><br>Use link ' + loginLink + ' to signin WeFlag account. <br><br>Thanks,<br>WeFlag Team.'
    }
    let response;
    response = await trans.sendMail(mailOpt);
    if (response.messageId) {
        response.status = 200;
    }
    else {
        response.status = 400;
    }
    return response;
}
export async function activeOrInactiveEmail(toEmail, status, text) {
    var mailOpt = {
        from: 'noreply.weflag@gmail.com',
        to: toEmail,
        subject: 'WeFlag - Account ' + status,
        html: 'Dear Member, <br><br> Greetings from WeFlag Team! <br><br><br>Your account has been ' + status + '' + text + '. <br><br>Thanks,<br>WeFlag Team.'
    }
    let response;
    response = await trans.sendMail(mailOpt);
    if (response.messageId) {
        response.status = 200;
    }
    else {
        response.status = 400;
    }
    return response;
}

export function emailValidation(email) {
    var emailSplit = email.toLowerCase().split("@");
    var s = "@" + emailSplit[1];
    if (freeEmailList.includes(s)) {
        return true
    }
    else {
        return false;
    }
}
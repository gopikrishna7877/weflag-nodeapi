export const noDataMsg = "No data available";
export const errorMsg = "Error";
export const companyIdMsg = "Company id is required";
export const serachTextMsg = "Search text is required";
export const maxThreeChr = "Minimum 3 characters is required";
export const fstsIdMsg = "Flag status id is required";
export const recuriterIdMsg = "Recruiter id is required";
export const recuriterNameMsg = "Recruiter name is required";
export const nameMsg = "Name is required";
export const emailMsg = "Email is required";
export const contactNumberMsg = "Contact number is required";
export const companyNameMsg = "Company Name is required";
export const whtMsg = "What happened is required";
export const errorInSaveMsg = "Error in save";
export const jobSeekerExistsMsg = "Job seeker exists";
export const jobSeekerIdMsg = "Job seeker id is required";
export const jobSeekerNotExistsMsg = "Job seeker not exists";
export const invalidOtpMsg = "Invalid OTP";
export const OtpExpireMsg = "OTP has expired";
export const userNotExistsMsg = "User not exists";
export const userExistsMsg = "User exists";
export const cityMsg = "City is required";
export const OtpSendToMailMsg = "Otp not send to email";
export const OtpMsg = "OTP is required";
export const OtpResendMsg = "OTP resend successfully";
export const emailErrMsg = "We are not accepting free mails";
export const OtpExpTime = 5;
export const recuriterNotExistMsg = "Recruiter not exists";
export const recuriterExistMsg = "Recruiter exists";
export const activeIdMsg = "Active status value is required";
export const adminActiveMsg = "Your account has been inactive, contact admin";
export const companyNotExistsMsg = "Company not exists";
export const companyNotExistsAdminMsg = "Company not exists, contact admin";
export const creditBLMsg = "Your credit balance is low, contact admin";
export const userNameMsg = "User Name is required";
export const passwordMsg = "Password is required";
export const adminErrorMsg = "Enter valid details";
export const companyExistsMsg = "Company is exists";
export const creditErrorMsg = "Credit value is required";
export const emailErrorMsg = "Email not send";
export const idMsg = "Id is required";
export const flagIdMsg = "Flag id is required";
export const invalidDetailsMsg = "You entered invalid details, contact admin";
export var noShowCredits = 5;
export var notJoinedCredits = 5;
export var joinedCredits = 5;
export var serachCredits = -1;
export const creditsIdMsg = "Id is required";
export const noShowValueMsg = "No Show value is required";
export const notJoinedValueMsg = "Not Joined value is required";
export const joinedValueMsg = "Joined value is required";
export const searchValueMsg = "Search value is required";

export const searchCrType = "Search";
export const noShowCrType = "No Show";
export const notJoinedCrType = "Not Joined";
export const joinedCrType = "Joined";

export const freeEmailList = [
    "@gmail.com",
    "@yahoo.com",
    "@yahoo.co.in",
    "@yahoo.co",
    "@zoho.com",
    "@outlook.com",
    "@hotmail.com",
    "@protonmail.com",
    "@protonmail.ch",
    "@tutanota.com",
    "@tutanota.de",
    "@tutamail.com",
    "@tuta.io",
    "@keemail.me",
    "@icloud.com",
    "@me.com",
    "@mac.com",
    "@aol.com",
    "@yandex.com",
    "@mail.com,",
    "@email.com,",
    "@myself.com",
    "@gmx.com",
    "@gmx.us",
    "@contbay.com",
    "@damnthespam.com",
    "@kurzepost.de",
    "@objectmail.com",
    "@proxymail.eu",
    "@rcpt.at",
    "@trash-mail.at",
    "@trashmail.at",
    "@trashmail.com",
    "@trashmail.net",
    "@wegwerfmail.de",
    "@wegwerfmail.net",
    "@wegwerfmail.org",
    "@safe-mail.net",
    "@lycos.com",
    "@hushmail.com",
    "@hush.com",
    "@mac.hush.com",
    "@fastmail.com"
]


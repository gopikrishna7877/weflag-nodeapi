"use strict";
exports.__esModule = true;
exports.noDataMsg = "No data available";
exports.errorMsg = "Error";
exports.companyIdMsg = "Company id is required";
exports.serachTextMsg = "Search text is required";
exports.maxThreeChr = "Minimum 3 characters is required";
exports.fstsIdMsg = "Flag status id is required";
exports.recuriterIdMsg = "Recruiter id is required";
exports.recuriterNameMsg = "Recruiter name is required";
exports.nameMsg = "Name is required";
exports.emailMsg = "Email is required";
exports.contactNumberMsg = "Contact number is required";
exports.companyNameMsg = "Company Name is required";
exports.whtMsg = "What happened is required";
exports.errorInSaveMsg = "Error in save";
exports.jobSeekerExistsMsg = "Job seeker exists";
exports.jobSeekerIdMsg = "Job seeker id is required";
exports.jobSeekerNotExistsMsg = "Job seeker not exists";
exports.invalidOtpMsg = "Invalid OTP";
exports.OtpExpireMsg = "OTP has expired";
exports.userNotExistsMsg = "User not exists";
exports.userExistsMsg = "User exists";
exports.cityMsg = "City is required";
exports.OtpSendToMailMsg = "Otp not send to email";
exports.OtpMsg = "OTP is required";
exports.OtpResendMsg = "OTP resend successfully";
exports.emailErrMsg = "We are not accepting free mails";
exports.OtpExpTime = 5;
exports.recuriterNotExistMsg = "Recruiter not exists";
exports.recuriterExistMsg = "Recruiter exists";
exports.activeIdMsg = "Active status value is required";
exports.adminActiveMsg = "Your account has been inactive, contact admin";
exports.companyNotExistsMsg = "Company not exists";
exports.companyNotExistsAdminMsg = "Company not exists, contact admin";
exports.creditBLMsg = "Your credit balance is low, contact admin";
exports.userNameMsg = "User Name is required";
exports.passwordMsg = "Password is required";
exports.adminErrorMsg = "Enter valid details";
exports.companyExistsMsg = "Company is exists";
exports.creditErrorMsg = "Credit value is required";
exports.emailErrorMsg = "Email not send";
exports.idMsg = "Id is required";
exports.flagIdMsg = "Flag id is required";
exports.invalidDetailsMsg = "You entered invalid details, contact admin";
exports.noShowCredits = 5;
exports.notJoinedCredits = 5;
exports.joinedCredits = 5;
exports.serachCredits = -1;
exports.creditsIdMsg = "Id is required";
exports.noShowValueMsg = "No Show value is required";
exports.notJoinedValueMsg = "Not Joined value is required";
exports.joinedValueMsg = "Joined value is required";
exports.searchValueMsg = "Search value is required";
exports.searchCrType = "Search";
exports.noShowCrType = "No Show";
exports.notJoinedCrType = "Not Joined";
exports.joinedCrType = "Joined";
exports.freeEmailList = [
    "@gmail.com",
    "@yahoo.com",
    "@yahoo.co.in",
    "@yahoo.co",
    "@zoho.com",
    "@outlook.com",
    "@hotmail.com",
    "@protonmail.com",
    "@protonmail.ch",
    "@tutanota.com",
    "@tutanota.de",
    "@tutamail.com",
    "@tuta.io",
    "@keemail.me",
    "@icloud.com",
    "@me.com",
    "@mac.com",
    "@aol.com",
    "@yandex.com",
    "@mail.com,",
    "@email.com,",
    "@myself.com",
    "@gmx.com",
    "@gmx.us",
    "@contbay.com",
    "@damnthespam.com",
    "@kurzepost.de",
    "@objectmail.com",
    "@proxymail.eu",
    "@rcpt.at",
    "@trash-mail.at",
    "@trashmail.at",
    "@trashmail.com",
    "@trashmail.net",
    "@wegwerfmail.de",
    "@wegwerfmail.net",
    "@wegwerfmail.org",
    "@safe-mail.net",
    "@lycos.com",
    "@hushmail.com",
    "@hush.com",
    "@mac.hush.com",
    "@fastmail.com"
];

"use strict";
exports.__esModule = true;
var APIError = /** @class */ (function () {
    function APIError(code, message) {
        if (code === void 0) { code = 400; }
        // super(message)
        this.msg = message;
        // this.key = key;
        this.status = code;
    }
    return APIError;
}());
exports.APIError = APIError;

import * as encryp from 'cryptr';
const cryptr = new encryp('123abc242526xyz');

export function encrypt(text) {
    return cryptr.encrypt(text);
}

export function decrypt(text) {
    return cryptr.decrypt(text);
}
